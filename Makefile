CC=gcc
PARAMS=-lm -Wall -lssl -lcrypto -Wpedantic -lc
FOLDER=build
CLIENT=build/client
SERVER=build/server

TARGET=cli srv

build_client: build_common cd_client cli

build_server: build_common cd_server srv

build_common: cd_common $(FOLDER)/common.o 

$(CLIENT)/%.o : client/%.c
	$(CC) -o $@ -c $< $(PARAMS)

$(SERVER)/%.o : server/%.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)/%.o : %.c
	$(CC) -o $@ -c $< $(PARAMS)

cli: $(FOLDER)/common.o $(addprefix $(CLIENT)/,idList.o state.o listSelect.o annuaire.o client.o connexion.o contact.o admin.o mainClient.o)
	$(CC) -o $@ $^ $(PARAMS)

srv: $(FOLDER)/common.o $(addprefix $(SERVER)/,csvManager.o serveur.o data.o access.o create.o get.o delete.o update.o connect.o mainServer.o)
	$(CC) -o $@ $^ $(PARAMS)

cd_client:
	mkdir -p $(CLIENT)
	cd $(CLIENT)

cd_server:
	mkdir -p $(SERVER)
	cd $(SERVER)

cd_common:
	mkdir -p $(FOLDER)
	cd $(FOLDER)

clean:
	mkdir -p $(FOLDER)
	rm -rf $(FOLDER)
	rm -f $(TARGET)

runAll:
	./serveur & sleep 1 && ./client
