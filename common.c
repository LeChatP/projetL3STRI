#define _GNU_SOURCE
#include "common.h"
#include <setjmp.h>

char **tmpRequete;

char *translateToCsvData(const char *source){
    char *data = strdup(source);
    data = sanitizeCharTo(data,'"',"\"\"");
    data = sanitizeCharTo(data,'\n',"\\n");
    size_t final_size = strlen(data)+2; // surround by " so add 2
    data = (char*)realloc(data,final_size+1);
    memmove(data+1,data,final_size-2);
    data[0] = '"';
    data[final_size-1] = '"';
    data[final_size] = '\0';
    if(!strcmp(data,"\"\""))*data = '\0';
    return data;
}

char *appendData(const char c,char* request,char *value){
    size_t size = strlen(request);
    size_t newsize = size+sizeof(c)+strlen(value);
    request = realloc(request,(newsize+1)*sizeof(char));
    request[size] = c;
    request[size+1]='\0';
    request = strcat(request,value);
    request[newsize] = '\0';
    return request;
}

char *appendMessageData(char* request,char *value){
    return appendData('/',request,value);
}
char *appendCsvData(char* request,char *value){
    return appendData(',',request,value);
}

char *endMessageLine(char *result){
    size_t size = strlen(result);
    result = realloc(result,(size+2)*sizeof(char));
    result[size] = '\n';
    result[size+1] = '\0';
    return result;
}

char *buildLine(const char delim,const int part, const char *first, va_list ap){
    char *result = NULL;
    if(delim == CCSV_DELIM){
        char *tmp = translateToCsvData(first);
        result=strdup(tmp);
        free(tmp);
    }else result = strdup(first);
    char *args = NULL;
    while ((args = va_arg(ap, char*)) != NULL ){
        if(delim == CCSV_DELIM){
            char *tmp = translateToCsvData(args);
            result = appendData(delim,result,tmp);
            free(tmp);
        }else result = appendData(delim,result,args);
    }
    if(!part){
        result = endMessageLine(result);
    }
    return result;
}

extern char *buildMessage(const char *part,...){
    va_list ap;
    va_start(ap,part);
    char *result = buildLine(CDELIM,0,part,ap);
    va_end(ap);
    return result;
}

extern char *buildPartMessage(const char *part,...){
    va_list ap;
    va_start(ap,part);
    char *result = buildLine(CDELIM,1,part,ap);
    va_end(ap);
    return result;
}

extern char *buildCsvLine(const char *part,...){
    va_list ap;
    va_start(ap,part);
    char *result = buildLine(CCSV_DELIM,1,part,ap);
    va_end(ap);
    return result;
}

/* evaluate if reponse has code specified, otherwise handle error code. */
/* return N-ième argument matching or 0 if error */
extern int checkResponse(char **reponse,char *code,...){
    char *retcode = firstArgument(reponse);
    char *message = nextArgument();
    int match = 1;
    va_list ap;
    va_start(ap, code);
    int i = 0;
    while(match && code != NULL){
        match = strcmp(retcode,code);
        code = va_arg(ap, char*);
        i++;
    }
    if(!match){
        return i;
    }else{
        switch(*retcode){
            case '0':
                printf("Réponse innatendue : %s\n",message);
                break;
            case '1':
                printf("Erreur de transmission : %s\n",message);
                break;
            case '2':
                printf("Erreur de connexion : %s\n",message);
                break;
            case '3':
                printf("Erreur d'existence : %s\n",message);
                break;
            case '4':
                printf("Erreur : %s\n",message);
                break;
            default:
                printf("Code d'erreur inconnu : %s\n",message);
        }
        return 0;
    }
}

char* sanitizeCharTo(char *str,char key,char *value){
    char *tmp = str;
	char *position = strchr(tmp,key);
	if(position != NULL){
		int pos = position-tmp;
		char *new_command = NULL;
		while(position != NULL){
			new_command = str_replace(tmp,pos,1,value);
			if(tmp != str)free(tmp); // free ancient string
			tmp = new_command;
			position = strchr(&tmp[pos+strlen(value)],key);
			pos = position-tmp;
		}
	}
	return tmp;
}

char *str_replace(const char *s, unsigned int start, unsigned int length,
			 const char *ct)
{
	char *new_s = NULL;
	size_t size = strlen(s);
	new_s = malloc(sizeof(*new_s) * (size - length + strlen(ct) + 1));
    if(new_s == NULL)goto free_error;
	if (new_s != NULL) {
		memmove(new_s, s, start);
		memmove(&new_s[start], ct, strlen(ct));
		memmove(&new_s[start + strlen(ct)], &s[start + length],
		       size - length - start + 1);
	}
    free_error:
	return new_s;
}

// from https://stackoverflow.com/questions/4235519/counting-number-of-occurrences-of-a-char-in-a-string-in-c
extern int lengthMessage(char *s){
    int i = 0;
    for (; s[i]; s[i]=='/' ? i++ : *s++);
    return i+1;
}

extern char *nextArgument(){
    return strsep(tmpRequete,SDELIM);
}

extern char *firstArgument(char **requete){
    (*requete)[strcspn(*requete, "\n")] = 0;
    tmpRequete = requete;
    return strsep(tmpRequete,SDELIM);
}

extern void emptystdin(){
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}