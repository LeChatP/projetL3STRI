
#ifndef IDLIST_H_
#define IDLIST_H_

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

typedef struct {
    char *key;
    int id;
    char *name;
} IdData; /*  structure permettant de lister des noms */

typedef struct IdList IdList;
struct IdList{
	IdList *next;
	IdData data;
};

/**
 * \brief ajoute un utilisateur a la list
 * \author Eddie BILLOIR
 * 
 * \param list le début de la liste 
 * \param user La donnée
 * \return IdList* la liste avec une nouvelle entrée
 */
extern IdList *append_IdData(IdList *list, IdData user);

/**
 * \brief Crée une liste d'utilisateurs
 * \author Eddie BILLOIR
 * 
 * \param data Une donnée initiale
 * \return IdList* Une liste avec la donnée initiale
 */
extern IdList *creer_idlist (IdData data);
/**
 * \brief renvoie le dernier élément de la liste en paramètre
 * \author Eddie BILLOIR
 * 
 * \param elem Le début de la liste
 * \return IdList* le pointeur du dernier élément
 */
extern IdList *dernierElement(IdList *elem);

/**
 * \brief Affiche la liste des éléments
 * \author Eddie BILLOIR
 * 
 * \param list le début de la liste
 */
extern void afficherElements(IdList *list);

/**
 * \brief Donne la taille de la liste
 * \author Eddie BILLOIR
 * 
 * \param list le début de la liste
 * \return int la taille de la liste
 */
extern int size_idList(IdList *list);

/**
 * \brief Donne nieme Element de la liste
 * \author Eddie BILLOIR
 * 
 * \param list début de la liste
 * \param position la position de l'objet dans la liste
 * \return IdData La structure de données
 */
extern IdData getElementList(IdList *list,int position);

/**
 * \brief Numérote de 1 à n les données des structures
 * 
 * \param elem Le début de la liste numérotée
 */
void setKeys(IdList *elem);

/**
 * \brief Libère la mémoire
 * \author Eddie BILLOIR
 * 
 * \param list Début de la liste
 */
void free_IdList(IdList *list);

/**
 * \brief Effectue le tri de la liste
 * 
 * \param debutRef Le pointeur du début de la liste
 */
extern void sort(IdList **debutRef);
/**
 * \brief Effectue Algorithme du quickSort
 * 
 * \param debut On donne le début de la tranche
 * \param fin la fin de la tranche
 * \return IdList* 
 */
IdList *quickSort(IdList *debut, IdList *fin);
/**
 * \brief Effectue le partitionnage pour le tri par QuickSort
 * 
 * \param debut On donne le début
 * \param fin la fin de la tranche
 * \param newDebut le nouveau début partiellement triée
 * \param newFin la nouvelle fin partiellement triée
 * \return IdList* 
 */
IdList *partition(IdList *debut, IdList *fin, IdList **newDebut, IdList **newFin);

#endif
