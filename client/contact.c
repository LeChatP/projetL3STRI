#define _GNU_SOURCE
#include "contact.h"
#include <setjmp.h>
#include <signal.h>
#include <sys/signalfd.h>

/**
 * \brief Annule la demande d'informations en cours avec SIGINT
 * \author Eddie BILLOIR
 * 
 * \param sig le signal
 */
void cancelAskContact(int sig);

/**
 * \brief Demande les inforamtions d'un contact, annulable
 * \author Joffrey RIEU SICART
 * 
 * \return Contact* 
 */
Contact *askContact();
jmp_buf buf;

void traitementContactVerbose(int choix){
    StateData *state = getState();
    if(strcmp(state->idUser,state->idAnnuaire)) return;
    switch (choix)
    {
    case 1:
        modifierContact(state->idContact);
        break;
    case 2:
        supprimerContact(state->idContact);
        break;
    default:
        break;
    }
}

void traitementListeContact(int choix){
    StateData *state = getState();
    IdData data = getElementList(state->dataList,choix);
    nextState(CONTACT_VERBOSE,NULL,NULL,data.key,NULL);
}

int getContactInfo(char *idAnnuaire,char *idContact){
    int res = 0;
    char *response = NULL;
    char *request = buildMessage("GET","CONTACT",idAnnuaire,idContact,NULL);
    if(!Emission(request))goto free_error;
    
    response = Reception();
    char *tmp = response;
    if(response == NULL || !checkResponse(&tmp,CDATA_CONTACT,NULL)){
        goto free_error;
    }
    res ++;
    //Version non conforme au standard C
    //<id>/<nom>/<prénom>/<mail>/<naissance>/<téléphone>/<code_postal>/<ville>/<adresse>/<remarque>
    //printf("Nom\t\t: %9$s\nPrénom \t\t: %8$s\nTéléphone\t: %7$s\nCode postal\t: %6$s\nVille\t\t: %5$s\nAdresse\t\t: %4$s\nEmail\t\t: %3$s\nNaissance\t: %2$s\nRemarque\t: %1$s\n",
    //    nextArgument(),nextArgument(),nextArgument(),nextArgument(),nextArgument(),nextArgument(),nextArgument(),nextArgument(),nextArgument());
    printf("Nom\t\t: %s\n", nextArgument());
    printf("Prénom \t\t: %s\n", nextArgument());
    printf("Mail\t\t: %s\n", nextArgument());
    printf("Naissance\t: %s\n", nextArgument());
    printf("Téléphone\t: %s\n", nextArgument());
    printf("Code Postal\t: %s\n", nextArgument());
    printf("Ville\t\t: %s\n", nextArgument());
    printf("Adresse\t\t: %s\n", nextArgument());
    char *rem = NULL;
    printf("Remarque\t: %s\n", (rem = nextArgument()) == NULL ?"":rem);
    free_error:
    if(request != NULL)free(request);
    if(response != NULL)free(response);
    return res;

}


int ajouterContact(){
    int i = 0;
    char *request = NULL;
    char *response = NULL;
    Contact *contact = askContact(0);
    if(contact == NULL) goto free_error;
    /* CREATE/CONTACT/<nom>/<prénom>/<mail>/<naissance>/<téléphone>/<code_postal>/<ville>/<adresse> */
    request = buildMessage("CREATE","CONTACT",contact->nom,contact->prenom,contact->mail,contact->naissance,contact->telephone,contact->codep,contact->ville,contact->adresse,contact->remarque,NULL);
    if (!Emission(request)){
        perror("Impossible d'ajouter le contact");
        goto free_error;
    }
    response = Reception(); /*  on récupère le nom du propriétaire */
    char *tmp = response;
    if(response== NULL) {
        perror("Impossible d'ajouter le contact");
        goto free_error;
    }
    if(checkResponse(&tmp,COK,NULL)){
        printf("Contact ajouté.\nRetour au menu principal\n");
        i=1;
    }else{
        perror("Impossible d'ajouter le contact");
    }
    free_error:
    if(contact != NULL)free(contact);
    if(request != NULL)free(request);
    if(response != NULL)free(response);
    return i;
}

void askField(char *field, char * message,int size, int required,int update){
    do{
        printf("%s%s :\n",message,update ? ", Laissez vide si aucun changement":"");
        getline(&field,(size_t*)&size,stdin);
        if(strchr(field,'\n')!=NULL)field[strcspn(field, "\n")] = 0;
        else emptystdin();
        if(*field!='\0')strcpy(field,sanitizeCharTo(field,'/',"."));
    }while(required && (field == NULL || !strcmp(field,"")) && !update);
}

/* respecting RFC 3696 for email address */
Contact *askContact(int update){
    struct sigaction action = {0};
    action.sa_handler = cancelAskContact;
    action.sa_flags   = SA_RESETHAND;
    if (sigaction(SIGINT, &action, NULL) == -1)
        perror("Impossible d'attribuer d'action à SIGINT");
    if (!sigsetjmp(buf,SIGINT)){
        Contact *contact =malloc(sizeof(Contact));
        if(contact == NULL) return NULL;
        askField(contact->nom,"Nom du contact",MAX_USERNAME,1,update);
        askField(contact->prenom,"Prénom du contact",MAX_USERNAME,1,update);
        askField(contact->mail,"Mail du contact (max 320)",MAX_EMAIL,1,update);
        askField(contact->naissance,"Date de naissance du contact (format dd.MM.YYYY) (optionnel)",MAX_NAISSANCE,0,update);
        askField(contact->telephone,"Numéro de téléphone du contact (optionnel)",MAX_PHONE,0,update);
        askField(contact->codep,"Code Postal du contact (optionnel)",MAX_CODEPOSTAL,0,update);
        askField(contact->ville,"Ville du contact (optionnel)",MAX_VILLE,0,update);
        askField(contact->adresse,"Adresse du contact (optionnel)",MAX_ADRESSE,0,update);
        askField(contact->remarque,"Remarque (optionnel)",MAX_REMARQUE,0,update);
        return contact;
    }else{
        return NULL;
    }
}

int modifierContact(char *idContact){
    int i = 0;
    char *response = NULL;
    char *request = NULL;
    Contact *contact =(Contact*)malloc(sizeof(Contact));
    if(contact == NULL) goto free_error;
    contact = askContact(1);
    if(contact == NULL) goto free_error;
    request = buildMessage("UPDATE","CONTACT",idContact,contact->nom,contact->prenom,contact->mail,contact->naissance,contact->telephone,contact->codep,contact->ville,contact->adresse,NULL);
    if (!Emission(request))goto free_error;
    response = Reception();
    if(response == NULL ) goto free_error;
    char *tmp = response;
    if(checkResponse(&tmp,COK,NULL)){
        printf("Contact mis à jour.\n");
        i++;
    }else{
        perror("Impossible d'ajouter ce contact");
    }
    free_error:
    if(contact != NULL)free(contact);
    if(request != NULL)free(request);
    if(response != NULL)free(response);
    return i;
}

int supprimerContact(char *idContact){
    int i = 0;
    char *response = NULL;
    char *request = buildMessage("DELETE","CONTACT",idContact,NULL);
    if (!Emission(request))goto free_error;
    response = Reception();
    char *tmp = response;
    if(response == NULL) goto free_error;
    if(checkResponse(&tmp,COK,NULL)){
        printf("Contact Supprimé.\n");
        i++;
    }else{
        perror("Impossible d'ajouter ce contact");
    }
    char *annuaire = strdup(getState()->idAnnuaire);
    restoreState();
    if(!getContactsAnnuaire(annuaire)) restoreState();
    free(annuaire);
    free_error:
    free(request);
    if(response != NULL)free(response);
    return i;
}

void cancelAskContact(int sig){
    printf("\nAnnulation...\n");
    siglongjmp(buf,1);
}
