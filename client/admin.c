#define _GNU_SOURCE
#include "admin.h"
#include "state.h"
#include <setjmp.h>
#include <signal.h>
#include <sys/signalfd.h>

/**
 * \brief Demande au client les informations de l'utilisateur.
 * \author Matthieu GALLEA
 * 
 * \param update 
 * \return User* 
 */
User *askUser(int update);

/**
 * \brief annule la demande d'informations lors du signal SIGINT
 * \author Eddie BILLOIR
 * 
 * \param sig 
 */
void cancelAskUser(int sig);
sigjmp_buf buf;

extern int ajouterUtilisateur(){
    int result = 0;
    User *user = askUser(0);
    char *emission = NULL;
    char *reponse = NULL;
    if(user == NULL) goto free_error;
    emission = buildMessage("CREATE","USER",user->Nom,user->Prenom,user->email,NULL);
    if(!Emission(emission)){
        setUserType(UNKNOWN);
        goto free_error;
    }
    reponse = Reception();
    char *tmp = reponse;
    if(tmp != NULL && checkResponse(&tmp,COK,NULL)){
        printf("L'identifiant de ce nouvel utilisateur : %s\n",nextArgument());
        result=1;
    }else{
        perror("Impossible de créer un utilisateur.");
    }
    free_error:
    if(emission != NULL){
        free(emission);
        if(reponse != NULL)free(reponse);
    }
    return result;
}

extern void listUsers(ETAT etat){
    IdList *list = NULL;
    if(executeList(etat,"GET/SERVER\n",CID_USER,&list))
        nextState(etat,NULL,NULL,NULL,list);
}

extern void modifierUtilisateur(){
    listUsers(UPDATE_USER);
}

void updateUser(int choix){
    StateData *state = getState();
    IdData data = getElementList(state->dataList,choix);
    User *user = askUser(1);
    char *reponse = NULL;
    char *request = NULL;
    if(user == NULL) goto free_error;
    printf("Réinitialiser le mot de passe (y/N) ?");
    char c = getchar();
    if(c!='\n')emptystdin();
    char *reset = "";
    if(c == 'y'){
        reset = "RESET";
    }
    request = buildMessage("UPDATE","USER",data.key,user->Nom,user->Prenom,user->email,reset,NULL);
    if(!Emission(request)){
        goto free_error;
    }
    reponse = Reception();
    char *tmp = reponse;
    if(reponse == NULL) goto free_error;
    switch(checkResponse(&tmp,COK,NULL)){
        case 1:
            printf("Utilisateur mis-à-jour!\n");
            break;
    }
    free_error:
    restoreState();
    if(request != NULL)free(request);
    if(reponse != NULL)free(reponse);
}

User *askUser(int update){
    struct sigaction action = {0};
    action.sa_handler = cancelAskUser;
    action.sa_flags   = SA_RESETHAND;
    if (sigaction(SIGINT, &action, NULL) == -1)
        perror("Impossible d'attribuer d'action à SIGINT");
    if (!sigsetjmp(buf,SIGINT)){
        User *result = malloc(sizeof(User));
        if(result == NULL) siglongjmp(buf,1);
        result->Nom = NULL;
        result->Prenom = NULL;
        result->email[0]= 0;
        char *updatestring = "laisser vide si aucun changement";
        printf("Nom de l'utilisateur %s:\n", update == 1 ? updatestring : "");
        size_t size = MAX_USERNAME;
        
        getline(&result->Nom,&size,stdin);
        result->Nom[strcspn(result->Nom, "\n")] = 0;
        result->Nom = sanitizeCharTo(result->Nom,'/',".");
        printf("Prénom de l'utilisateur %s:\n",update == 1 ? updatestring : "");
        getline(&result->Prenom,&size,stdin);
        result->Prenom[strcspn(result->Prenom, "\n")] = 0;
        result->Prenom = sanitizeCharTo(result->Prenom,'/',".");
        printf("Adresse Email (320 caractères max) %s:\n",update == 1 ? updatestring : "");
        fgets(result->email,MAX_EMAIL,stdin);
        result->email[strcspn(result->email, "\n")] = 0;
        strcpy(result->email,sanitizeCharTo(result->email,'/',"."));
        return result;
    }else{
        return NULL;
    }
   
}

extern void supprimerUtilisateur(){
    listUsers(DELETE_USER);
}

void deleteUser(int choix){
    StateData *state = getState();
    IdData data = getElementList(state->dataList,choix);
    char *emission = buildMessage("DELETE","USER",data.key,NULL);
    if(!Emission(emission)){
        goto free_error;
    }
    char *reponse = Reception();
    if(reponse == NULL)goto free_error;
    char *tmp = reponse;
    if(reponse != NULL && checkResponse(&tmp,COK,NULL)){
        printf("Utilisateur a été supprimé\n");
    }else{
        perror("Impossible de supprimer cet utilisateur.\n");
    }
    restoreState();
    free_error:
    if(emission != NULL){
        free(emission);
        if(reponse != NULL)free(reponse);
    }
    //TODO SIGSETJMP
}

void cancelAskUser(int sig){
    printf("\nAnnulation...\n");
    siglongjmp(buf,1);
}