/* Documentation tag for Doxygen
 */

/*! \mainpage Documentation Projet Annuaire
 *
 * \section intro_sec Introduction
 *
 * Ce système de client serveur d'annuaire partagé est un projet de L3 STRI a pour but de proposer une RFC avec un service d'annuaires, de clients, d'utilisateurs partagés sur un serveur.
 * Il est disponible en accès en concurrence et offre divers possibilités.
 *
 * \section fonctions_sec Fonctionnalités
 * 
 * * Multi-utilisateurs (accès en concurrence)
 * * Liste par ordre alphabétique (algorithme quicksort)
 * * Annulation de l'ajout / mise à jour d'information par CTRL+C
 * * Ajout, modification, suppression des utilisateur pour les administrateurs
 * * Ajout, modification, suppression des contacts pour les propriétaires
 * * Autoriser, révoquer les accès à son annuaire
 * * Création / suppression de son annuaire, avec ses accès
 * * Hachage des mots de passe par SHA256
 * 
 * \section install_sec Installation
 * 
 * A la racine du projet, dans un bash linux faire : 
 * 
 * ```Bash
 *    ./configure.sh
 * ```
 * 
 * Puis compiler le serveur : 
 * ```Bash
 *    make build_server
 * ```
 * 
 * Puis compiler le client :
 * ```Bash
 *    make build_client 
 * ```
 * 
 * Deux fichiers ont été crées : ./srv et ./cli à la racine
 *
 * \subsection util_sec Utilisation
 * 
 * Peu importe l'ordre de démarrage, il faut lancer ./cli et ./srv <BR/>
 * 
 * Une fois lancé l'adresse du serveur est demandé par le client. il faut alors le renseigner. il est possible de spécifier un port en particulier en collant ':' <BR/>
 * 
 * Le logiciel vous demandera vos identifiants de connexion, une fois connecté vous aurez accès au service. <BR/>
 *  
 * Lors de la prenière connexion, le mot de passe entré sera celui défini pour l'utilisateur. Seul un administrateur pourra réinitialiser ce mot de passe. <BR/>
 * 
 * Pour modifier un mot de passe en tant qu'administrateur il faut aller dans le menu modifier un utiisateur et répondre 'y' lorsque le client propose de réinitialiser le mot de passe <BR/>
 * 
 * Pour autoriser un utiliateur dans son annuaire, il faut connaître son identifiant. <BR/>
 * 
 * Lors de la création d'un utilisateur, l'identifiant de l'utilisateur est donné a la fin, il faudra le noter et le divulguer a la personne concernée. <BR/>
 * 
 * Un utilisateur, administrateur ou non n'a par défaut pas d'annuaire, il peut en créer un. Après la création d'un annuaire il deviendra propriétaire et administrateur si il l'était précédemment. <BR/>
 * 
 * \subsection conf_sec Configuration
 * 
 * Pour mettre son utilisateur en administrateur il faut aller dans le fichier data/users.csv et changer la 3eme valeur de la ligne de votre utilisateur de 0 à 1
 *
 * \section dev_sec Développement
 * 
 * Nous avons fourni un maximum d'effort pour que le serveur n'ait pas de fuite de mémoire
 * Dû à un manque de temps, il n'a pas été possible que le client n'ait aucune fuite mémoire, l'effort à été fourni pour le rendre le plus ergonomique possible.
 * 
 * Le système utilise les fonctions a nombre variable d'arguments pour construire les requetes et réponses, mais également les ligne CSV
 * 
 * Pour gérer les actions sur les fichiers CSV une liste de fonctions a été crée reproduisant un comportement similaire aux requêtes SQL, simplifiant les actions du serveur, et offrant une stabilité et une rapidité pour l'ajout de nouvelles fonctionnalités
 * 
 * Il est possible pour le client d'annuler l'action en cours, nous avons donc utilisé les signaux avec les sigsetjmp et siglongjmp
 * 
 * Le système gère également le blocage des fichiers en édition, ainsi si deux client souhaitent modifier un même fichier, l'un des deux attendra que l'autre finisse son édition.
 * 
 * Nous avons développé avec le module Live Share, module permettant de programmer tous ensemble en temps réel (comme Google Docs).
 * 
 * \section stats_sec Statistiques
 * 
 * * + de 100 fonctions
 * * 10 structures
 * * 38 fichiers C et H
 * * 21 malloc, 11 realloc
 * * 4 fonctions à paramètres variable
 * * 3 pointeurs de fonctions
 * 
 * \section copyright Copyright (C) BILLOIR Eddie, GALLEA Matthieu, RIEU SICART Joffrey

    This program is free software: you can redistribute it and/or modify
    
    it under the terms of the GNU General Public License as published by
    
    the Free Software Foundation, either version 3 of the License, or
    
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    
    GNU General Public License for more details.


    You should have received a copy of the GNU General Public License
    
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * <BR><BR>
 *
 */
#define _GNU_SOURCE
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <ctype.h>

#include "idList.h"
#include "connexion.h"
#include "contact.h"
#include "annuaire.h"
#include "admin.h"
#include "state.h"

/**
 * \brief Affiche le menu générique
 * \author Eddie BILLOIR
 * 
 */
void afficherMenu(ETAT,USERTYPE);
/**
 * \brief Affiche le menu principal en fonction du type d'utilisateur (admin utilisateur...)
 * \author Matthieu GALLEA
 * 
 */
void afficherMenuPrincipal(USERTYPE);

/**
 * \brief Demande les informations du serveur
 * \author Eddie BILLOIR
 * 
 */
void traitementDisconnected();

/**
 * \brief Traitement de menu principal en tant qu'utilisateur
 * \author Eddie BILLOIR
 * 
 * \param touche la touche entrée
 */
void traitementUser(char touche);

/**
 * \brief Traitement du menu principal en tant que propriétaire d'un annuaire
 * \author Eddie BILLOIR
 * 
 * \param touche 
 */
void traitementOwner(char touche);

/**
 * \brief Traitement du menu principal en tant qu'administrateur sans annuaire
 * \author Eddie BILLOIR
 * 
 * \param touche 
 */
void traitementAdminUser(char touche);

/**
 * \brief Traitement du menu principal en tant qu'administrateur avec un annuaire
 * \author Eddie BILLOIR
 * 
 * \param touche 
 */
void traitementAdminOwner(char touche);

/**
 * \brief Choix Client inconnu
 * \author Matthieu GALLEA
 * 
 */
void unknownChoice();

/**
 * \brief Demande de déconnexion
 * \author Joffreu RIEU SICART
 * 
 */
void deconnecter();

char address[MAX_DNS+1],port[MAX_PORT_CHARS+1];

void afficherMenu(ETAT etat,USERTYPE usertype){
    switch(etat){
        case DISCONNECTED:
            break;
        case NOT_AUTH:
            printf("La connexion a été perdue, Vueillez vous identifier : ");
            break;
        case MAIN_MENU:
            afficherMenuPrincipal(usertype);
            break;
        case LIST_BOOK:
            if(getState()->dataList == NULL) goto error_menu;
            printf("Entrez un nombre pour afficher les contacts d'un annuaire\n");
            afficherElements(getState()->dataList);
            printf("r : Revenir en arrière\nm : Retourner au menu principal\nq : Quitter l'application\n");
            break;
        case LIST_CONTACT:
            if(getState()->dataList == NULL) goto error_menu;
            printf("Entrez un nombre pour afficher les informations d'un contact :\n");
            afficherElements(getState()->dataList);
            printf("r : Revenir en arrière\nm : Retourner au menu principal\nq : Quitter l'application\n");
            break;
        case CONTACT_VERBOSE:
            if(getState()->dataList == NULL) goto error_menu;
            printf("Informations du contact :\n");
            if(getContactInfo(getState()->idAnnuaire,getState()->idContact)){
                if(!strcmp(getState()->idAnnuaire,getState()->idUser)) printf("Sélectionnez une action : \n1 : Modifier ce contact\n2 : Supprimer ce contact\nr : Revenir en arrière\nm : Retourner au menu princpal\nq : Quitter l'application\n");
                else printf("Sélectionnez une action : \nr : Revenir en arrière\nm : Retourner au menu princpal\nq : Quitter l'application\n");
            }
            break;
        case ACCESS_REVOKE:
            if(getState()->dataList == NULL) goto error_menu;
            printf("Liste des utilisateurs autorisés, entrez un nombre pour retirer cet utilisateur de la liste :\n");
            afficherElements(getState()->dataList);
            printf("r : Revenir en arrière\nm : Retourner au menu principal\nq : Quitter l'application\n");
            break;
        case UPDATE_USER:
        case DELETE_USER:
            if(getState()->dataList == NULL) goto error_menu;
            printf("Entrez un nombre pour %s cet utilisateur :\n",etat == DELETE_USER ? "Supprimer":"Mettre-à-jour");
            afficherElements(getState()->dataList);
            printf("r : Revenir en arrière\nm : Retourner au menu principal\nq : Quitter l'application\n");
            break;
        default:
            printf("Vous êtes dans un état inconnu. Impossible d'afficher le menu\n");
    }
    return;
    error_menu:
    printf("Etat impossible, aucune liste en cours...\n");
    return;
}

void afficherMenuPrincipal(USERTYPE usertype){
    switch(usertype){
        case USER:
            printf("Entrez une valeur pour faire un choix :\n1 : Liste des annuaires\n2 : Créer un annuaire\nq : Quitter\n");
            break;
        case OWNER:
            printf("Entrez une valeur pour faire un choix :\n1 : Liste des annuaires\n2 : Ajouter un contact\n3 : Autoriser un utilisateur à l'annuaire\n4 : Révoquer les accès d'un utilisateur à l'annuaire\n5 : Supprimer son annuaire\nq : Quitter l'application\n");
            break;
        case ADMIN*USER:
            printf("Entrez une valeur pour faire un choix :\n1 : Liste des annuaires\n2 : Créer un annuaire\n3 : Ajouter un Utilisateur\n4 : Mettre-à-jour un Utilisateur\n5 : Supprimer un utilisateur\nq : Quitter l'application\n");
            break;
        case ADMIN*OWNER:
            printf("Entrez une valeur pour faire un choix :\n1 : Liste des annuaires\n2 : Ajouter un contact\n3 : Autoriser un utilisateur à l'annuaire\n4 : Révoquer les accès d'un utilisateur à l'annuaire\n5 : Ajouter un Utilisateur\n6 : Mettre-à-jour un Utilisateur\n7 : Supprimer un utilisateur\n8 : Supprimer son annuaire\nq : Quitter l'application\n");
            break;
        default:
            printf("Vous êtes dans un état inconnu. Impossible d'afficher le menu\n");
    }
}

void actionMenu(ETAT etat,USERTYPE usertype){
    if(etat == DISCONNECTED){
        traitementDisconnected();
    } /*  in case of disconnexion, just reconnect */
    else if(etat == NOT_AUTH) traitementConnexion();/* when not auth ask connexion */
    else if(etat == MAIN_MENU) { /* get input for main menu */
        char c = getchar();
        if(c!='\n')emptystdin();
        switch(usertype){
            case USER:
                traitementUser(c);
                break;
            case OWNER:
                traitementOwner(c);
                break;
            case ADMIN*USER:
                traitementAdminUser(c);
                break;
            case ADMIN*OWNER:
                traitementAdminOwner(c);
                break;
            default:
                perror("Une erreur a été détectée, déconnexion...");
                Terminaison();
                getState()->etat = DISCONNECTED;
                break;
        }
    }else{ /* input for list menu */
        if(getState()->dataList == NULL){
            restoreState();
            return;
        }
        int nDigits = floor(log10(abs(size_idList(getState()->dataList))))+1, i = 0;
        char *strchoix = malloc(nDigits+1);
        if(strchoix == NULL){
            perror("Impossible d'allouer la mémoire pour la séléction d'action");
            return;
        }
        memset(strchoix,0,nDigits+1);
        do{ /*  tant qu'on rentre des numéros et qu'il y a la possibilité d'entrer un nombre plus grand */
            strchoix[i] = getchar(); /* alors on entre un nombre plus grand */
            i++;
        }while(isdigit((int)strchoix[i])&&nDigits>i);
        if(strchoix[i]!='\n')emptystdin();
        if(isdigit(strchoix[i-1])){ /* quand le chiffre a été sélectionné */
            int choix = atoi(strchoix);
            switch(etat){
                case LIST_BOOK:
                    traitementListeAnnuaire(choix);
                    break;
                case LIST_CONTACT:
                    traitementListeContact(choix);
                    break;
                case CONTACT_VERBOSE:
                    traitementContactVerbose(choix);
                    break;
                case ACCESS_REVOKE:
                    revokerUser(choix);
                    break;
                case UPDATE_USER:
                    updateUser(choix);
                    break;
                case DELETE_USER:
                    deleteUser(choix);
                    break;
                default:
                    unknownChoice();
                    break;
            }
        }else{ /* case when user choose to quit, or go to main menu, or to go back */
            switch(strchoix[i-1]){
                case 'q':
                    Terminaison();
                    getState()->etat = QUIT;
                    break;
                case 'm':
                    returnToMainMenu();
                    break;
                case 'r':
                    restoreState();
                    break;
                default:
                    unknownChoice();
                    break;
            }
        }
        if(strchoix !=NULL) free(strchoix);
    }
}

/**
 * \brief Fonction du programme principal
 * 
 * \return int code de retour
 * \todo ajouter les options getopt
 */
int main(){
    traitementDisconnected();
    initState(NOT_AUTH);
    printf("Veuillez vous identifier : \n");
    traitementConnexion();
    ETAT etat = getState()->etat;
    do{
        afficherMenu(etat,getUserType());
        actionMenu(etat,getUserType());
    }while((etat = getState()->etat) != QUIT);
}

void traitementDisconnect(){
    Emission("DISCONNECT\n");
    Terminaison();
    initState(QUIT);
}

void traitementDisconnected(){
    if(*address == '\0'){
        do{
            char newadresse[MAX_DNS+MAX_PORT_CHARS+2],*newport;
            printf("\nQuel est l'adresse du serveur ? (par défaut : localhost:7050)\n");
            fgets(newadresse,MAX_DNS+MAX_PORT_CHARS+1,stdin);
            newadresse[strcspn(newadresse, "\n")] = 0;
            if(*newadresse == '\0'){
                strcpy(address,DEFAULT_ADDRESS);
                strcpy(port,DEFAULT_PORT);
            }else if((newport = strrchr(newadresse,':')) != NULL){
                *newport = '\0';
                newport ++;
                strcpy(address,newadresse);
                strcpy(port,newport);
            }else{
                strcpy(address,newadresse);
                strcpy(port,DEFAULT_PORT);
            }
        }while(!InitialisationAvecService(address, port));
    }else{
        if(!InitialisationAvecService(address,port)){
            *address = '\0';
            *port = '\0';
            traitementDisconnected();
        }
    }

}

void traitementUser(char touche){
    switch (touche)
    {
        case '1':
            listerAnnuaires();
            break;
        case '2':
            creerAnnuaire();
            break;
        case 'q':
            traitementDisconnect();
            break;
        default:
            unknownChoice();
            break;
    }
}

void traitementOwner(char touche){
    switch (touche)
    {
        case '1':
            listerAnnuaires();
            break;
        case '2':
            ajouterContact();
            break;
        case '3':
            grantUser();
            break;
        case '4':
            listerAccess();
            break;
        case '5':
            supprimerAnnuaire();
            break;
        case 'q':
            traitementDisconnect();
            break;
        default:
            unknownChoice();
    }
}

void traitementAdminUser(char touche){
    switch (touche)
    {
        case '1':
            listerAnnuaires();
            break;
        case '2':
            creerAnnuaire();
            break;
        case '3':
            ajouterUtilisateur();
            break;
        case '4':
            modifierUtilisateur();
            break;
        case '5':
            supprimerUtilisateur();
            break;
        case 'q':
            traitementDisconnect();
            break;
        default:
            unknownChoice();
    }
}

void traitementAdminOwner(char touche){
    switch (touche)
    {
        case '1':
            listerAnnuaires();
            break;
        case '2':
            ajouterContact();
            break;
        case '3':
            grantUser();
            break;
        case '4':
            listerAccess();
            break;
        case '5':
            ajouterUtilisateur();
            break;
        case '6':
            modifierUtilisateur();
            break;
        case '7':
            supprimerUtilisateur();
            break;
        case '8':
            supprimerAnnuaire();
            break;
        case 'q':
            traitementDisconnect();
            break;
        default:
            unknownChoice();
    }
}

void unknownChoice(){
    printf("Commande inconnue...\n");
}
