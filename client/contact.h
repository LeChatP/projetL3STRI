#ifndef CONTACT_H_
#define CONTACT_H_

#include "../common.h"
#include "client.h"
#include "state.h"

/**
 * \brief Structure d'un contact
 * \author Matthieu GALLEA
 * 
 */
typedef struct {
    char *contactID, nom[MAX_USERNAME+1], prenom[MAX_USERNAME+1],mail[MAX_EMAIL+1],naissance[MAX_NAISSANCE+1],telephone[MAX_PHONE+1],codep[MAX_CODEPOSTAL+1],ville[MAX_VILLE+1],adresse[MAX_ADRESSE],remarque[MAX_REMARQUE+1];
} Contact;

/**
 * \brief traitement lors d'une sélection 
 * \author Eddie BILLOIR
 * 
 * \param choix 
 */
void traitementListeContact(int choix);
/**
 * \brief traitement dans le menu d'un contact
 * \author Matthieu GALLEA
 * 
 * \param choix Le choix de l'utilisateur
 */
void traitementContactVerbose(int choix);

/**
 * \brief Demande la liste des contacts d'un annuaire
 * \author Eddie BILLOIR
 * 
 * \param idAnnuaire l'id dmemandé
 */
int getContactsAnnuaire(char *id);
/**
 * \brief Demande et affiche les informations du contact
 * \author Matthieu GALLEA
 * 
 * \param idAnnuaire L'annuaire du contact
 * \param idContact L'identifiant du contact
 * \return int 
 */
int getContactInfo(char *idAnnuaire,char *idContact);

/**
 * \brief Demande l'ajout d'un contact
 * \author Matthieu GALLEA
 * 
 * \return int 0 si erreur 1 si OK
 */
int ajouterContact();
/**
 * \brief Demande la modifiaction d'un contact
 * \author Matthieu GALLEA
 * 
 * \return int 
 */
int modifierContact();
/**
 * \brief Demande la suppression d'un contact
 * \author Matthieu GALLEA
 * 
 * \return int 
 */
int supprimerContact();

#endif