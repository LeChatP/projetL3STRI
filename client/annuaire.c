#define _GNU_SOURCE
#include "annuaire.h"
#include <setjmp.h>
#include <signal.h>
sigjmp_buf buf;

/**
 * \brief annule la demande d'informations lors du signal SIGINT
 * \author Eddie BILLOIR
 * 
 * \param sig 
 */
void cancelGrantUser(int sig);

/* lors de la sélection d'un annuaire dans une liste */
extern void traitementListeAnnuaire(int choice){
    StateData *state = getState();
    IdData data = getElementList(state->dataList,choice);
    getContactsAnnuaire(data.key);
}

extern void creerAnnuaire(){
    Emission("CREATE/BOOK\n");
    char *response = Reception();
    char *tmp = response;
    if(response == NULL || !checkResponse(&tmp,COK,NULL)){
        goto free_error;
    }
    setUserType(getUserType()==ADMIN*USER ? ADMIN*OWNER : OWNER);
    printf("Votre annuaire a été crée\n");
    free_error:
    free(response);
}

extern void listerAnnuaires(){
    IdList *list = NULL;
    if(executeList(LIST_BOOK,"GET/BOOKLIST\n",CID_BOOK,&list)) 
        nextState(LIST_BOOK,NULL,NULL,NULL,list);
}

void grantUser(){
    char username[BUFSIZ];
    char *request = NULL;
    char *reponse = NULL;
    struct sigaction action =  {0};
    action.sa_handler = cancelGrantUser;
    action.sa_flags   = SA_RESETHAND;
    if (sigaction(SIGINT, &action, NULL) == -1)
        perror("Impossible d'attribuer d'action à SIGINT");
    if (!sigsetjmp(buf,SIGINT)){
        printf("Quel est le nom d'utilisateur a dont il faut donner accès ?\n");
        fgets(username,BUFSIZ,stdin);
        username[strcspn(username, "\n")] = 0;
        request = buildMessage("ACCESS","GRANT",username,NULL);
        if(!Emission(request)){
            goto free_error;
        }
        reponse =  Reception();
        char *tmp = reponse;
        if(reponse == NULL) goto free_error;
        switch(checkResponse(&tmp,COK,CERR_INEXISTENT_USER,CERR_ALREADY_IN_LIST,NULL)){
            case 1:
                printf("Utilisateur ajouté dans la liste!\n");
                break;
            case 2:
                printf("Cet Utilisateur n'existe pas.\n");
                break;
            case 3:
                printf("Cet utilisateur est déjà dans la liste\n");
                break;
        }
    }
    free_error:
    if(request != NULL)free(request);
    if(reponse != NULL)free(reponse);
}

void revokerUser(int choix){
    StateData *state = getState();
    IdData data = getElementList(state->dataList,choix);
    char *request = buildMessage("ACCESS","REVOKE",data.key,NULL);
    if(!Emission(request)){
        goto free_error;
    }
    char *reponse = Reception();
    char *tmp = reponse;
    if(reponse == NULL) goto free_error;
    switch(checkResponse(&tmp,COK,NULL)){
        case 1:
            printf("Utilisateur retiré de la liste!\n");
            break;
    }
    restoreState();
    free_error:
    if(request != NULL)free(request);
    if(reponse != NULL)free(reponse);
}

int getContactsAnnuaire(char *id){
    int res = 0;
    char *requete = buildMessage("GET","BOOK",id,NULL);
    nextState(LIST_CONTACT,NULL,id,NULL,NULL);
    IdList *list = NULL;
    if(executeList(LIST_CONTACT,requete,CID_CONTACT,&list)){
        getState()->dataList = list;
        res++;
    }else{
        restoreState();
    }
    if(requete != NULL) free(requete);
    return res;
}

extern void listerAccess(){
    IdList *list = NULL;
    if(executeList(ACCESS_REVOKE,"GET/USERS\n",CID_USER,&list))
        nextState(ACCESS_REVOKE,NULL,NULL,NULL,list);
}

extern void supprimerAnnuaire(){
    Emission("DELETE/BOOK\n");
    char *response = Reception();
    char *tmp = response;
    if(response == NULL || !checkResponse(&tmp,COK,NULL)){
        goto free_error;
    }
    setUserType(getUserType() == ADMIN*OWNER ? ADMIN*USER : USER);
    printf("Votre annuaire a été supprimé\n");
    free_error:
    free(response);
}

void cancelGrantUser(int sig){
    printf("\nAnnulation...\n");
    siglongjmp(buf,1);
}