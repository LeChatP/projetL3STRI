#ifndef ANNUAIRE_H_
#define ANNUAIRE_H_

#include "../common.h"
#include "client.h"
#include "state.h"
#include "listSelect.h"

/**
 * \brief Effectue le traitement pour les requetes de demande d'annuaires
 * \author Matthieu GALLEA
 * 
 * \param choice 
 */
extern void traitementListeAnnuaire(int choice);

/**
 * \brief Récupère les Contacts d'un Annuaire
 * \author Matthieu GALLEA, corrigé par Eddie BILLOIR
 * 
 */
extern int getContactsAnnuaire();

/**
 * \brief Get the Id Annuaire
 * \author Matthieu GALLEA, corrigé par Eddie BILLOIR
 * 
 * \return char* 
 */
extern char *getIdAnnuaire();

/**
 * \brief Crée un annuaire avec les paramètres de la requete
 * \author Matthieu GALLEA, corrigé par Eddie BILLOIR
 * 
 */
extern void creerAnnuaire();

/**
 * \brief Liste les annuaires de l'utilisateur
 * \author Eddie BILLOIR
 * 
 */
extern void listerAnnuaires();

/**
 * \brief Liste les Id utilisateurs ayant accès à l'annuaire utilisateur
 * \author Eddie BILLOIR
 * 
 */
extern void listerAccess();

/**
 * \brief Supprime l'annuaire de l'utilisateur et change sont type de propritétaire a utilisateur
 * \author Matthieu GALLEA, corrigé par Eddie BILLOIR
 * 
 */
extern void supprimerAnnuaire();

/**
 * \brief Ajoute l'utilisateur à la liste des accès
 * \author Eddie BILLOIR
 * 
 */
void grantUser();
void revokerUser(int choix);

#endif