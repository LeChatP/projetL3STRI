#ifndef CONNEXION_H_
#define CONNEXION_H_

#include "../common.h"
#include "client.h"
#include "state.h"

/**
 * \brief effectue la demande de connexion
 * \author Joffrey RIEU SICART
 */
extern void traitementConnexion();

/**
 * \brief demande les identifiants et effectue la connexion
 * \author Joffrey RIEU SICART
 */
extern USERTYPE connexion(char *username);

/**
 * \brief effectue la demande de déconnexion
 * \author Joffrey RIEU SICART
 */
extern void deconnecter();

/**
 * \brief hache les données du mot de passe
 * \author internet
 */
void sha256_string(char *string, char outputBuffer[SHA256_SIZE+1]);

#endif