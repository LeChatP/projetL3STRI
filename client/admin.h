#ifndef ADMIN_H_
#define ADMIN_H_

#include "../common.h"
#include "client.h"
#include "listSelect.h"

typedef struct {
    char *userName, *Nom, *Prenom,email[MAX_EMAIL+1];
} User;

/**
 * \brief Demande l'ajout d'un utilisateur
 * \author Eddie BILLOIR
 * 
 * \return int 1 si OK 0 si POK
 */
extern int ajouterUtilisateur();

/**
 * \brief Affiche la liste des utilisateurs du serveur pour sélectionner lequel modifier
 * \author Eddie BILLOIR
 * 
 */
extern void modifierUtilisateur();

/**
 * \brief Affiche la liste des utilisateurs du serveur pour sélectionner lequel supprimer
 * \author Eddie BILLOIR
 * 
 */
extern void supprimerUtilisateur();

/**
 * \brief Mets a jour l'utilisateur sélectionné
 * \author Eddie BILLOIR
 * 
 * \param choix 
 */
void updateUser(int choix);

/**
 * \brief Demande la suppression de l'utilisateur
 * \author Eddie BILLOIR
 * 
 * \param choix 
 */
void deleteUser(int choix);

/**
 * \brief Demande la liste des utilisateurs et met a jour l'état avec cette liste.
 * \author Eddie BILLOIR
 * 
 * \param etat 
 */
extern void listUsers(ETAT etat);

#endif