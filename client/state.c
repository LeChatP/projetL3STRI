#define _GNU_SOURCE
#include "state.h"

void freeStates();
void freeState(StateData *state);

StateData *states[MAX_DEPTH];
USERTYPE type;
int actualDepth;

StateData *getState(){
    return states[actualDepth];
}

USERTYPE getUserType(){
    return type;
}

void setUserType(USERTYPE ut){
    type = ut;
}

StateData *initState(const ETAT etat){
    if(*states != NULL) freeStates();
    StateData *data = (StateData*)malloc(sizeof(StateData));
    actualDepth = 0;
    if(data == NULL) return NULL;
    data->etat = etat;
    data->idAnnuaire = NULL;
    data->idContact = NULL;
    data->idUser = NULL;
    data->dataList = NULL;
    states[actualDepth] = data;
    return data;
}

StateData *cloneStateData(StateData *data){
    StateData *newData = (StateData*) malloc(sizeof(StateData));
    if(newData == NULL){
        perror("Impossible de passer à l'état suivant");
        return NULL;
    }
    newData->etat = data->etat;
    newData->idAnnuaire = NULL;
    newData->idContact = NULL;
    newData->idUser = NULL;
    newData->dataList = NULL;
    if(data->idAnnuaire!=NULL){
        newData->idAnnuaire=strdup(data->idAnnuaire);
    }
    if(data->idUser!=NULL){
        newData->idUser=strdup(data->idUser);
    }
    if(data->idContact !=NULL){
        newData->idContact=strdup(data->idContact);
    }
    if(data->dataList !=NULL){
        newData->dataList=data->dataList;
    }
    return newData;
}

StateData *nextState(ETAT etat,char *idUser, char *idAnnuaire,char *idContact, IdList *choices){
    if (actualDepth < MAX_DEPTH-1){
        states[actualDepth+1] = cloneStateData(states[actualDepth]);
        actualDepth++;
        states[actualDepth]->etat = etat;
        if(idUser != NULL)
            states[actualDepth]->idUser=strdup(idUser);
        if(idAnnuaire != NULL)
            states[actualDepth]->idAnnuaire=strdup(idAnnuaire);
        if(idContact != NULL)
            states[actualDepth]->idContact=strdup(idContact);
        if(choices != NULL)
            states[actualDepth]->dataList=choices;
        return states[actualDepth];
    }
    return NULL;
}

StateData *restoreState(){
    if(actualDepth > 0){
        freeState(states[actualDepth]);
        actualDepth--;
    }
    return states[actualDepth];
}

StateData *returnToMainMenu(){
    while(states[actualDepth]->etat != MAIN_MENU){
        restoreState();
    }
    return states[actualDepth];
}

void freeStates(){
    for(int i = 0;i<actualDepth;i++){
        if(states[i]!=NULL)freeState(states[i]);
    }
    actualDepth = 0;
}

void freeState(StateData *state){
    if(state->idAnnuaire !=NULL)free(state->idAnnuaire);
    state->idAnnuaire = NULL;
    if(state->idContact !=NULL)free(state->idContact);
    state->idContact = NULL;
    if(state->idUser !=NULL)free(state->idUser);
    state->idUser = NULL;
    if(state != NULL)free(state);
    state = NULL;
}