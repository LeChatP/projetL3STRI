#include "connexion.h"
#include <openssl/sha.h>
#include <termios.h>

/**
 * \brief Récupère le nom et prénom du client qui vient de se connecter
 * 
 * \return int retourne si la requete a réussi ou non
 */
int welcome(); 

void traitementConnexion(){
    ETAT usertype = UNKNOWN;
    char username[BUFSIZ];
    while((usertype = connexion(username))==UNKNOWN){
        if(getState()->etat == DISCONNECTED) return;
        printf("Veuillez réessayer\n");
    }
    setUserType(usertype);
    nextState(MAIN_MENU,username,NULL,NULL,NULL);
}

USERTYPE connexion(char *username){
    USERTYPE etat = UNKNOWN;
    char password[SHA256_SIZE+1];
    printf("Nom d'utilisateur : ");
    fgets(username,BUFSIZ,stdin); /*  demande nom d'utilisateur */
    if(username == NULL){
        goto free_error;
    }
    username[strcspn(username, "\n")] = 0;
    char *getpas = getpass("Mot de passe : ");
    sha256_string(getpas,password);
    char *message = buildMessage("CONNECT",username,password,NULL);
    if(!Emission(message)){
        goto free_error;
    }
    
    char *response = Reception();
    char *tmp = response;
    if(response == NULL){
        goto free_error;
    }
    switch (checkResponse(&tmp,COK,CERR_ALREADY_CONNECTED,NULL))
    {
    case 2:
        etat = getState()->etat;
    case 0:
        goto free_error;
    default:
        break;
    }
    char *token = nextArgument();
    if(!strcmp(token,"USER")){
        etat = USER;
    }else if(!strcmp(token,"OWNER")){
        etat = OWNER;
    }else {
        perror("Etat inconnu, non authentifié.");
        goto free_error;
    }
    token = nextArgument();
    if(!strncmp(token,"ADMIN",5)){
        etat *= ADMIN;
    }
    if(!welcome())perror("Impossible de récupérer votre nom et votre prénom");
    free_error:
    if(message != NULL){
        free(message);
        if(response != NULL)free(response);
    }
    return etat;
}

int welcome(){
    int retour = 0;
    if(!Emission("GET/USER\n"))goto free_error;
    char *response = Reception();
    char *tmp = response;
    if(response != NULL && checkResponse(&tmp,CDATA_CLIENT,NULL)){
        printf("Bienvenue %s %s\n",nextArgument(),nextArgument());
        retour = 1;
    }else
        goto free_error;
    free_error:
    if(response != NULL)free(response);
    return retour;

}

void deconnecter(){
    EmissionBinaire("DISCONNECT\n",MAX_DISCONNECT);
    setUserType(UNKNOWN);
    initState(DISCONNECTED);
}

/* Génère un hash pour le mot de passe entré */
/* obtenu depuis https://stackoverflow.com/questions/2262386/generate-sha256-with-openssl-and-c */
void sha256_string(char *string, char outputBuffer[SHA256_SIZE+1])
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, string, strlen(string));
    SHA256_Final(hash, &sha256);
    int i = 0;
    for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }
    outputBuffer[SHA256_SIZE] = '\0';
}