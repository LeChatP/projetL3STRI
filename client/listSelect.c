#define _GNU_SOURCE
#include "listSelect.h"
#include "idList.h"

/**
 * \brief Remplit les données de type contact
 * \author Eddie BILLOIR
 * 
 * \param idAnnuaire l'id de l'annuaire
 * \param idContact et l'id du contact
 * \return IdData* Les informations en une seule ligne
 */
IdData *fillContactInfo(char *idAnnuaire, char *idContact);
/**
 * \brief Remplit les informations d'un utilisateur en une liste de la liste
 * 
 * \param idUser identifiant de l'utilisateur 
 * \param defaultFormat Le format d'affichage par défaut
 * \param yourFormat le format d'affichage lorsque l'utilisateur est le client
 * \param unknownFormat le format d'affichage lorsque l'utilisateur n'a pas de nom/informations
 * \return IdData* 
 */
IdData *fillUserInfo(char * idUser, char* defaultFormat, char *yourFormat, char *unknownFormat);

IdData *fillInfo(ETAT nouvelEtat, char *token){
    switch(nouvelEtat){
        case LIST_BOOK:
            return fillUserInfo(token,"Annuaire de %s %s","#Votre annuaire","Annuaire d'un inconnu");
        case LIST_CONTACT:
            return fillContactInfo(getState()->idAnnuaire,token);
        case UPDATE_USER:
        case DELETE_USER:
        case ACCESS_REVOKE:
            return fillUserInfo(token,"%s %s","#Vous","Inconnu");
    }
    return NULL;
}

IdData *fillContactInfo(char *idAnnuaire, char *idContact){
    char *request = NULL;
    char *response = NULL;
    IdData *choice = malloc(sizeof(IdData));
    if(choice == NULL) goto free_error;
    request = buildMessage("GET","CONTACT",idAnnuaire,idContact,NULL);
    if (!Emission(request))goto free_error;
    response = Reception(); /*  on récupère le nom du propriétaire */
    char *tmp = response;
    if(response == NULL) goto free_error;
    if(checkResponse(&tmp,CDATA_CONTACT,NULL)){
        char* Nom = nextArgument();
        char* Prenom = nextArgument();
        size_t nameSize = strlen(Prenom)+strlen(Nom)+2;
        choice->name = malloc(nameSize);
        if(choice->name == NULL) goto free_error;
        sprintf(choice->name,"%s %s",Prenom,Nom); /* on y place le Prenom, le Nom et l'identifiant, au cas où il y a plusieurs le même nom/prénom */
        choice->name[nameSize-1] = '\0'; /*  on s'assure que la chaine se termine correctement */
        choice->key = strdup(idContact);
    }else {
        choice->name = strdup("Contact sans nom");
        choice->key = strdup(idContact);
    }
    free_error:
    if(request!=NULL)free(request);
    if(response!=NULL)free(response);
    return choice;
}

IdData *fillUserInfo(char * token, char* defaultFormat, char *yourFormat, char *unknownFormat){
    char *response = NULL;
    char *getUserRequest = NULL;
    IdData *choice = malloc(sizeof(IdData));
    if(choice == NULL) goto free_error;
    getUserRequest = buildMessage("GET","USER",token,NULL);
    if (!Emission(getUserRequest)) goto free_error;
    response = Reception(); /*  on récupère le nom du propriétaire */
    if(response == NULL) goto free_error;
    char *tmp = response;
    char* Prenom;
    char* Nom;
    size_t nameSize;
    switch(checkResponse(&tmp,CDATA_USER,CDATA_CLIENT,NULL)){
        case 1: /*  contenu d'un autre utilisateur */
            Prenom = nextArgument();
            Nom = nextArgument();
            nameSize = strlen(defaultFormat)-4+strlen(Prenom)+strlen(Nom)+1;
            choice->name = malloc(nameSize);
            if(choice->name == NULL) goto free_error;
            sprintf(choice->name,defaultFormat,Prenom,Nom,token); /* on y place le Prenom, le Nom et l'identifiant, au cas où il y a plusieurs le même nom/prénom */
            choice->name[nameSize-1] = '\0'; /*  on s'assure que la chaine se termine correctement */
            choice->key = strdup(token);
            break;
        break;
        case 2: /*  contenu comprenant des infos perso */
            choice->name = strdup(yourFormat);
            choice->key = strdup(token);
            break;
        default:
            choice->name= strdup(unknownFormat);
            choice->key = strdup(token);
            goto free_error;
    }
    free_error:
    if(getUserRequest != NULL)free(getUserRequest);
    if(response != NULL)free(response);
    return choice;
}

void printNothingMessage(ETAT nouvelEtat){
    switch(nouvelEtat){
        case LIST_BOOK:
            printf("Vous n'avez accès à aucun annuaire\n");
            break;
        case LIST_CONTACT:
            printf("Il n'y a aucun contact dans cet annuaire\n");
            break;
        case ACCESS_REVOKE:
            printf("Personne n'a accès à votre annuaire\n");
            break;
        }
}

int executeList(ETAT nouvelEtat,char *requete,char *waited_result, IdList **choice){
    int result = 0;
    char **tokens = NULL;
    char *response = NULL;
    if(!Emission(requete))goto free_error;
    response = Reception();
    char *tmp = response;
    if(response == NULL) goto free_error;
    int length = lengthMessage(response);
    if(!checkResponse(&tmp,waited_result,NULL)){
        goto free_error;
    }
    if(length < 3){
        printNothingMessage(nouvelEtat);
        goto free_error;
    }
    int step = 5;
    int i = 0;
    tokens = malloc(sizeof(char*)*step);
    if(tokens == NULL)goto free_error;
    char *token;
    while((token = nextArgument()) != NULL && ++i){
        if(i%step == 0)tokens = realloc(tokens,i+step*sizeof(char*)); // si i atteint un multiple de 5, on rajoute 5 dans le tableau
        tokens[i-1] = token;
    }
    for(int j = 0 ; j<i;j++){ /* on crée la liste pour le menu de sélection */
        if(*choice == NULL) 
            *choice = creer_idlist(
                *fillInfo(nouvelEtat,tokens[j]));
        else append_IdData(*choice,*fillInfo(nouvelEtat,tokens[j])); /*  place le nom du propriétaire de l'annuaire */
    }
    sort(choice); /*  une fois le tout récupéré, on trie le résultat avec un quicksort */
    setKeys(*choice); /* une fois trié on attribue un nombre à toutes les entrées */
    result = 1;
    free_error:
    if(tokens != NULL) free(tokens);
    if(response != NULL)free(response);
    return result;
}