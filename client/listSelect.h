#ifndef _LISTSELECT_H_
#define _LISTSELECT_H_

#include "../common.h"
#include "state.h"
#include "client.h"

/**
 * \brief Execute le tâches lors d'une selection dans une liste IdList
 * 
 * \param nouvelEtat 
 * \param requete 
 * \param waited_result 
 * \param choice 
 * \return int 
 */
int executeList(ETAT nouvelEtat,char *requete,char *waited_result, IdList **choice);

#endif