#include "idList.h"
/* crée une nouvelle liste */
IdList *creer_idlist (IdData data)
{
	IdList *list = malloc(sizeof(IdList));
	if (list)
 	{
		list->data = data;
		list->next = NULL;
	}
	return list;
}

void free_IdList(IdList *list){
	IdList *elem = list;
	while (elem != NULL){
		free(elem->data.key);
		free(elem->data.name);
		elem = elem->next;
	}
	free(list);
}

/* ajoute un élément a la liste */
IdList *append_IdData(IdList *list, IdData data)
{
	IdList **plist = &list;
	while (*plist)
		plist = &(*plist)->next;
	*plist = creer_idlist(data);
	if (*plist)
		return list;
	else
		return NULL;
}

/*  renvoie le dernier element de la liste */
IdList *dernierElement(IdList *list)
{
	IdList *elem = list;
	while (elem != NULL && elem->next != NULL)
		elem = elem->next;
	return elem;
}

/* tri du tableau */
void sort(IdList **debutRef)
{
	(*debutRef) = quickSort(*debutRef, dernierElement(*debutRef));
	return;
}

/* attribue les entiers pour chaque entrée selon son ordre */
void setKeys(IdList *elem){
    int i = 1;
    while (elem != NULL){
        elem->data.id=i;
		i++;
		elem = elem->next;
    }
}
 
/* tri par quicksort */
IdList *quickSort(IdList *debut, IdList *fin)
{
	/*  debut n'est pas null ni égal à la fin */
	if (!debut || debut == fin)
		return debut;
 
	IdList *newDebut = NULL, *newFin = NULL;
 
	/*  récupération du pivot + moitié gauche */
	IdList *pivot = partition(debut, fin, &newDebut, &newFin);
 
 	/* si le pivot est déjà le plus petit élement pas besoin d'itérer la première moitié */
	if (newDebut != pivot)
	{
		/*  on coupe le fil temporairement de l'élément précédant le pivot */
		IdList *tmp = newDebut;
		while (tmp->next != pivot)
			tmp = tmp->next;
		tmp->next = NULL;

 
		/*  on tri la première moitié : debut -> pivot -1 */
		newDebut = quickSort(newDebut, tmp);

 
		/*  on replace le fil */
		tmp = dernierElement(newDebut);
		tmp->next =pivot;
	}
 
	/*  tri de la seconde moitié */
	pivot->next = quickSort(pivot->next, newFin);

	return newDebut;
}

/*  partitionne la liste partant de la fin comme pivot */
/*  retourne le pivot + les nouvelles positions de debut et fin */
IdList *partition(IdList *debut, IdList *fin, IdList **newDebut, IdList **newFin)
{
	IdList *pivot = fin;
	IdList *prev = NULL, *cur = debut, *bout = pivot;
	/* tant que le pivot et cur ne se rencontrent pas */
	while (cur != pivot)
	{
		/*  on vérifie que cur < pivot à l'aide de strcmp */
		if (strcmp(cur->data.name,pivot->data.name)<0)
		{
			/*  on échange la valeur plus faible en tant que debut */
			if (*newDebut == NULL)
				*newDebut = cur;
			prev = cur;
			cur = cur->next;
		}
		/*  si cur est plus grand */
		else
		{
			/*  on déplace cur après le bout et on réattribue le bout en tant que cur */
			if (prev)
				prev->next = cur->next;
			IdList *tmp = cur->next;
			cur->next = NULL;
			bout->next = cur;
			bout = cur;
			cur = tmp;
		}
	}
	/* si le pivot etait déjà le plus petit element, on l'attribue a newDebut */
	if (*newDebut == NULL)
		*newDebut = pivot;
 
	/*  et on attribue le bout en tant que fin de la partition */
	(*newFin) = bout;
 
	/*  retourne le pivot partiellement/complètement trié */
	return pivot;
}

extern int size_idList(IdList *id){
	int result = 0;
	IdList *list = id;
	while (list != NULL){
		result++;		
		list = list->next;
    }
	return result;
}


extern void afficherElements(IdList *id){
	IdList *list = id;
    while (list != NULL){
        printf("%d : %s (%s)\n",list->data.id,list->data.name,list->data.key);
		list = list->next;
    }
}

extern IdData getElementList(IdList *plist,int position){
	int i = 0;
	IdList *list = plist;
	while (list != NULL && i < position-1){
        i++;
		list = list->next;
    }
	return list->data;
}