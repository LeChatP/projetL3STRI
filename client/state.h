#ifndef STATE_H_
#define STATE_H_

#include "../common.h"
#include "idList.h"

#define MAX_DEPTH 5

/**
 * \brief Etat du logiciel, permet de restaurer et avancer dans les états.
 * \author Eddie BILLOIR
 * 
 */
typedef struct sStateData {
    ETAT etat;
    char *idUser;
    char *idAnnuaire;
    char *idContact;
    IdList *dataList;
}StateData;

/**
 * \brief Obtenir l'état du programme
 * \author Joffrey RIEU SICART
 * 
 * \return StateData* 
 */
StateData *getState();

/**
 * \brief Crée le premier état du programme
 * \author Joffrey RIEU SICART
 * 
 * \param etat Le prenier état 
 * \return StateData* 
 */
StateData *initState(const ETAT etat);

/**
 * \brief Effectue un clone et avance d'un état.
 * \author Eddie BILLOIR
 * 
 * \param etat nouvel état
 * \param idUser nouvel utilisateur peut etre null
 * \param idAnnuaire nouvel annuaire peut etre NULL
 * \param idContact nouveau contact peut etre NULL
 * \param liste nouvelle liste crée
 * \return StateData* Nouvelle structure avec les champs rensignés + précédents
 */
StateData *nextState(ETAT etat,char *idUser, char *idAnnuaire,char *idContact,IdList *);

/**
 * \brief Retourne à létat précédent
 * \author Joffrey RIEU SICART
 * 
 * \return StateData* le nouvel état
 */
StateData *restoreState();

/**
 * \brief Retourne au menu principal
 * \author Joffrey RIEU SICART
 * 
 * \return StateData* 
 */
StateData *returnToMainMenu();

/**
 * \brief applique le nouveau type de l'utlisateur (utilisateur, propriétaire, admin...)
 * \author Joffrey RIEU SICART
 * 
 * \param ut Type de l'urilisateur
 */
void setUserType(USERTYPE ut);

/**
 * \brief Obtient le type de l'utilisateur
 * \author Joffrey RIEU SICART
 * 
 * \return USERTYPE 
 */
USERTYPE getUserType();

#endif