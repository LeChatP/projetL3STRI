var searchData=
[
  ['read',['READ',['../csvManager_8h.html#ada74e7db007a68e763f20c17f2985356',1,'csvManager.h']]],
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['reception',['Reception',['../client_8c.html#a2cf23ed330a4725a6139c6e126cb3064',1,'Reception():&#160;client.c'],['../client_8h.html#a2cf23ed330a4725a6139c6e126cb3064',1,'Reception():&#160;client.c'],['../serveur_8c.html#a2cf23ed330a4725a6139c6e126cb3064',1,'Reception():&#160;serveur.c'],['../serveur_8h.html#a2cf23ed330a4725a6139c6e126cb3064',1,'Reception():&#160;client.c']]],
  ['receptionbinaire',['ReceptionBinaire',['../client_8c.html#a8b26418c149e57e5fa406b80c207440d',1,'ReceptionBinaire(char *donnees, size_t tailleMax):&#160;client.c'],['../client_8h.html#a8b26418c149e57e5fa406b80c207440d',1,'ReceptionBinaire(char *donnees, size_t tailleMax):&#160;client.c'],['../serveur_8c.html#a8b26418c149e57e5fa406b80c207440d',1,'ReceptionBinaire(char *donnees, size_t tailleMax):&#160;serveur.c'],['../serveur_8h.html#a8b26418c149e57e5fa406b80c207440d',1,'ReceptionBinaire(char *donnees, size_t tailleMax):&#160;client.c']]],
  ['remarque',['remarque',['../structContact.html#ad798b9427ee01bb1f4fe36dbd6827cc3',1,'Contact::remarque()'],['../structContact.html#a34e92affce13393bdb9fb5aaddb00190',1,'Contact::remarque()']]],
  ['replacelineinfile',['replaceLineInFile',['../mainServer_8c.html#a3493a41a80a3644567bae89175040e64',1,'mainServer.c']]],
  ['restorestate',['restoreState',['../state_8c.html#a53e5bde48f466c533f30334c4475710d',1,'restoreState():&#160;state.c'],['../state_8h.html#a53e5bde48f466c533f30334c4475710d',1,'restoreState():&#160;state.c']]],
  ['returntomainmenu',['returnToMainMenu',['../state_8c.html#ae9d8bab4de1bed3b8679f813725cb2ca',1,'returnToMainMenu():&#160;state.c'],['../state_8h.html#ae9d8bab4de1bed3b8679f813725cb2ca',1,'returnToMainMenu():&#160;state.c']]],
  ['revokeruser',['revokerUser',['../annuaire_8c.html#a4b2b0882667e318342a042a72d1ee3a9',1,'revokerUser(int choix):&#160;annuaire.c'],['../annuaire_8h.html#a4b2b0882667e318342a042a72d1ee3a9',1,'revokerUser(int choix):&#160;annuaire.c']]],
  ['rwrite_5fcsv',['rwrite_csv',['../csvManager_8c.html#a56cbb43bd385f502414cf3e32ef53628',1,'csvManager.c']]]
];
