var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvw",
  1: "cisu",
  2: "acdgilmrsu",
  3: "abcdefghilmnopqrstuw",
  4: "abcdefgiklmnprstuvw",
  5: "ceisu",
  6: "_abcdeflmnoqrstuw",
  7: "dlp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Structures de données",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Définitions de type",
  6: "Macros",
  7: "Pages"
};

