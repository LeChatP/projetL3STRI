#define _GNU_SOURCE
#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdarg.h>

#define QUIT (ETAT) -1
#define DISCONNECTED (ETAT)0
#define NOT_AUTH (ETAT)1
#define MAIN_MENU (ETAT)2
#define LIST_BOOK (ETAT)3 /*  selection des annuaires */
#define LIST_CONTACT (ETAT)4 /*  selection des contacts */
#define CONTACT_VERBOSE (ETAT)5 /* Action sur un contact */
#define ACCESS_REVOKE (ETAT)6 /* seléction de l'utilisateur a revoquer */
#define UPDATE_USER (ETAT)7 /* Mise-à-jour d'un utilisateur */
#define DELETE_USER (ETAT)8 /* Suppression d'un utilisateur */

#define UNKNOWN (USERTYPE)0
#define ADMIN (USERTYPE)2 /* multiplicateur de USER ou OWNER */
#define USER (USERTYPE)2 /*  4 if admin user */
#define OWNER (USERTYPE)3 /*  6 if admin owner */

typedef char ETAT;
typedef char USERTYPE;

#define MAX_CONNECT 88 /*  MAX PDU length for CONNECT */
#define MAX_USERNAME 256 /*  MAX USERNAME length, respecting PDU */
#define MAX_USERID 20
#define MAX_DISCONNECT 11 /*  MAX DISCONNECT length, respecting PDU */
#define MAX_EMAIL 320 /*  Max EMAIL size */
#define SHA256_SIZE 64 /*  SHA256 SIZE FORMAT  */
#define MAX_DNS 253 /* MAX DNS Address */
#define MAX_PORT_CHARS 5 /* SIZE MAX FOR PORT */
#define MAX_NAISSANCE 10
#define MAX_PHONE 12
#define MAX_CODEPOSTAL 5
#define MAX_VILLE 254
#define MAX_ADRESSE 1024
#define MAX_REMARQUE 2048
#define MAX_COLUMNS 10

#define DEFAULT_PORT "7050"
#define DEFAULT_ADDRESS "localhost"

/*  codes de retour */
#define COK "000"
#define CASK_VALIDATION "001"
#define CID_USER "002"
#define CID_BOOK "003"
#define CID_CONTACT "004"
#define CDATA_CONTACT "005"
#define CDATA_USER "006"
#define CDATA_CLIENT "007"

// erreurs
#define CERR_UNABLE_EXEC "403"

#define CERR_HAS_BOOK "300"
#define CERR_ALREADY_IN_LIST "303"
#define CERR_NOT_IN_LIST "304"
#define CERR_INEXISTENT_USER "305"

#define CERR_WRONG_CREDENTIALS "207"
#define CERR_ALREADY_CONNECTED "208"

#define CERR_UNKOWN_REQUEST "100"
#define CERR_FEW_ARGUMENTS "101"
#define CERR_ACCESS_DENIED "104"

#define SDELIM "/"
#define SCSV_DELIM ","
#define CDELIM '/'
#define CCSV_DELIM ','

#define QUIT_KEY 'q'

#define BOOKDIR "./data/books/"
#define ACCESSFILE "./data/access.csv"
#define USERSFILE "./data/users.csv"
#define CSVEXTENSION ".csv"

/**
 * \brief Replace string s at start with length to ct
 * 
 * \param s string to replace
 * \param start position to replace
 * \param length length of replacement
 * \param ct string placed
 * \return char* 
 */
char *str_replace(const char *s, unsigned int start, unsigned int length, const char *ct);

/**
 * \brief This function will replace key to value in str
 * 
 * \param str string to be checked
 * \param key key search
 * \param value string replaced
 * \return char* 
 */
char* sanitizeCharTo(char *str,char key,char *value);

/**
 * \brief Permet de construire un message entier séparant les arguments par /
 * terminant le message par \\n
 * \author Eddie BILLOIR
 * 
 * \param message au moins un argument obligatoire pour créer un message
 * \param ... autorise autant d'arguments que possible, se terminant par NULL
 * \return char* le message crée
 */
extern char *buildMessage(const char *message,...);

/**
 * \brief Permet de construire une ligne Csv entiere en respectant l'echappement des caractères
 * \author Eddie BILLOIR
 *  
 * \param message au moins un argument obligatoire pour créer un message
 * \param ... autorise autant d'arguments que possible, se terminant par NULL
 * \return char* la ligne crée
 */
extern char *buildCsvLine(const char *part,...);

/**
 * \brief Termine un message crée
 * \author Eddie BILLOIR
 * 
 * \param result le message
 */
extern char *endMessageLine(char *result);

/**
 * \brief Permet d'ajouter une chaine au message déjà crée
 * \author Eddie BILLOIR
 * 
 * \param request la requete existante
 * \param value la valeur a rajouter 
 * \return char* la chaine modifiée
 */
extern char *appendMessageData(char* request,char *value);

/**
 * \brief Permet d'ajouter une chaine à la ligne csv déjà crée
 * \author Eddie BILLOIR
 * 
 * \param request la ligne existante
 * \param value la valeur a rajouter 
 * \return char* la chaine modifiée
 */
extern char *appendCsvData(char* request,char *value);

/**
 * \brief Cette Fonction met en forme la donnée afin qu'elle soit adéquat pour le format CSV
 * 
 * \param source source à mettre en forme
 * \return char* résultat mis en forme
 */
char *translateToCsvData(const char *source);

/**
 * \brief Permet de construire une partie de message séparant les arguments par /
 * \author Eddie BILLOIR
 * 
 * \param message au moins un argument obligatoire pour créer un message
 * \param ... autorise autant d'arguments que possible, se terminant par NULL
 * \return char* le message crée
 */
extern char *buildPartMessage(const char *part,...);

/**
 * \brief Permet de vérifier les deux premiers arguments d'un message avec une liste de codes corrects possible
 * \author Eddie BILLOIR
 * 
 * \param reponse 
 * \param code liste de codes de retour possible (pas d'erreurs)
 * \param ... 
 * \return int 0 si code d'erreur ou le dernier chiffre du code de retour + 1
 * \todo améliorer la valeur de retour avec sscanf
 */
extern int checkResponse(char **reponse,char *code,...);

/**
 * \brief Effectue un strsep de la requete
 * Cette fonction sert simplement à la lisibilité du code
 * \author Eddie BILLOIR
 * 
 * \param requete 
 * \return char* 
 */
extern char *firstArgument(char **requete);

/**
 * \brief Effectue un strsep de la variable en first argument
 * Cette fonction sert simplement à la lisibilité du code
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
extern char *nextArgument();

/**
 * \brief Récupère la longueur du message
 * 
 * \param s le message
 * \return int nombre d'arguments d'un message
 */
extern int lengthMessage(char *s);

/**
 * \brief Vide le buffer stdin si scanf a été utilisé dessus
 * \author Eddie BILLOIR
 * 
 * \return int 
 */
extern void emptystdin();

#endif