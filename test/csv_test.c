#include "../server/csvManager.h"

typedef struct {
    char *username,*password,*nom,*prenom,*email;
} UserData;

typedef struct {
    UserData *users;
    int number;
} UserList;

void print(int row, int col, char* data){
    printf("ligne :%d, colonne : %d, donnée : %s\n",row,col,data);
}

int main(){
    CsvWhere where;
    where.col = DEFAULT;
    where.line = DEFAULT;
    where.field = NULL;
    CsvSelect cols;
    *cols.cols=DEFAULT;
    cols.nb_cols=1;
    CsvTable *result = select_csv(&cols,"test.csv",where);
    if(result == NULL){
        printf("PAs de réponse\n");
        return 0;
    }
    CsvTable *tmp = result;
    do{
        char **array = tmp->line;
        while(*array !=NULL){
            printf("%s",*array);
            free(*array);
            array++;
        };
        CsvTable *buf = tmp;
        tmp = tmp->next;
        free(buf->line);
        free(buf);
        printf("\n");
    }while(tmp!=NULL);
    //free(result);
    where.col = 1;
    where.line = 1;
    write_csv("test.csv",where,"My edited\" data");

}

