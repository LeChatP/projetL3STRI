#ifndef CREATE_H_
#define CREATE_H_

#include "data.h"

/**
 * \brief Traite toutes les requetes CREATE
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementCREATE();

#endif