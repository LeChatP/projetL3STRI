#ifndef DATA_H_
#define DATA_H_

#define _GNU_SOURCE
#include "../common.h"
#include "csvManager.h"

// ces structures sont differentes de celle du client car le programme serveur ne stocke pas la donnée brute, il manipule les pointeurs des données reçues
typedef struct {
    char *userName, *nom, *prenom, *email;
} User;

typedef struct {
    char *contactID, *nom, *prenom,*mail,*naissance,*telephone,*codep,*ville,*adresse,*remarque;
} Contact;

/**
 * \brief Vérifie si l'annuaire appartient à la session courante
 * \author Eddie BILLOIR
 * 
 * \param id idannuaire
 * \return int 1 si oui 0 sinon
 */
int isOwner(char *id);

/**
 * \brief Obtient le chemin de l'annuaire
 * Note : Vider la mémoire après utilisation
 * \author Eddie BILLOIR
 * 
 * \param idBook 
 * \return char* 
 */
char *getBook(char *idBook);
/**
 * \brief Message de refus d'accès
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *accessDeniedMessage();

/**
 * \brief obtient la variable globale si l'utilisateur est déconnecté
 * \author Eddie BILLOIR
 * 
 * \return int 
 */
int getDisconnected();

/**
 * \brief Change la variable globale de déconnexion. 
 * Supprime le fork, et la communication (socket) a la fin du traitement de la requete.
 * \author Eddie BILLOIR
 * 
 * \param value 
 */
void setDisconnected(int value);

/**
 * \brief Obtient l'identifiant de l'utilisateur de la session courante (sous-processus).
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *getUsername();

/**
 * \brief Met le nom d'utilisateur pour la session
 * \author Eddie BILLOIR
 * 
 * \param username 
 */
void setUsername(char *username);

/**
 * \brief Libère la mémoire du nom d'utilisateur
 * \author Eddie BILLOIR
 */
void freeUsername();

/**
 * \brief Obtient le type d'utilisateur de la session
 * 
 * \return USERTYPE 
 */
USERTYPE getUsertype();

/**
 * \brief Set the Usertype object
 * \author Eddie BILLOIR
 * 
 * \param type 
 */
void setUsertype(USERTYPE type);

/**
 * \brief Obtient une structure Where où toutes les valeurs sont par défaut
 * \author Eddie BILLOIR
 * 
 * \return CsvWhere 
 */
CsvWhere getWhereDefault();

#endif