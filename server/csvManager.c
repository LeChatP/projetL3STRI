/**
 * \file csv_manager.c
 * Gestionnaire de fichiers CSV
 * Avec des fonction de SELECT FROM WHERE, INSERT, UPDATE (en équivelence)
 * \author Eddie BILLOIR
 * \brief Fichier permettant d'effectuer des requetes sur des fichiers csv
 */
#define  _GNU_SOURCE
#include <stdlib.h>
#include <sys/file.h> 
#include <string.h>
#include "csvManager.h"

/**
 * \brief ouvre le fichier demandé avec les paramètres de CSV
 * \author Eddie BILLOIR
 * 
 * \param file_name nom du fichier
 * \param mode mode READ ou WRITE
 * \return FILE* 
 */
FILE *open_file(char * file_name, int mode);

/**
 * \brief Ajoute une colonne (chaine de caractères) dans le tableau
 * \author Eddie BILLOIR
 * 
 * \param line 
 * \param value 
 */
void append_data(CsvTable *line,char*value);

/**
 * \brief Vérifie si la colonne est dans la sélection
 * \author Eddie BILLOIR
 * 
 * \param select 
 * \param actual_field 
 * \return int renvoie 1 si la valeur est sélectionnée
 */
int selectprocess(CsvSelect *select, int actual_field);

/**
 * \brief récupère les colonnes sélectionées selon la condition where
 * \author Eddie BILLOIR
 * 
 * \param string 
 * \param select 
 * \param where 
 * \return CsvTable* 
 */
CsvTable *n_field(char *string,CsvSelect *select, CsvWhere where);

/**
 * \brief Vérifie si la colonne match avec le where
 * \author Eddie BILLOIR
 * 
 * \param where
 * \param col 
 * \param token 
 * \return int 1 si match 0 sinon
 */
int checkWhere(CsvWhere where,int col,char *token);

/**
 * \brief Set la colonne sur la ligne string
 * \author Eddie BILLOIR
 * 
 * \param file 
 * \param string 
 * \param num_field 
 * \param value 
 * \return int 
 */
int set_field(FILE *file, char **string, int num_field, char *value);

/**
 * \brief Set la ligne en fin de fichier
 * \author Eddie BILLOIR
 * 
 * \param file 
 * \param file_name 
 * \param position 
 * \param value 
 * \return int 
 */
int set_line(FILE* file,char* file_name,CsvWhere position, char *value);


/**
 * \brief Lock FILE
 * 
 * \param fd file to lock
 * \return int 
 * \todo utilisation de ftcnl() pour la gestion de NFS
 */
int lock_file(FILE *fd);

/**
 * \brief Unlock FILE 
 * 
 * \param fd file to unlock
 * \return int 
 */
int unlock_file(FILE *fd);


int lock_file(FILE *fd)
{
  return flock(fileno(fd), LOCK_SH);
}

int unlock_file(FILE *fd)
{
  return flock(fileno(fd),LOCK_UN);
}

void freeTable(CsvTable *table){
    if(table == NULL) return;
    CsvTable *tmp = table;
    int cpt = 0;
    do{
        for(int i = 0; i<tmp->nb_cols;i++){
            char *strbuf = *(tmp->line+i);
            if(strbuf!=NULL)free(strbuf);
        };
        if((*tmp).line!=NULL)free((*tmp).line);
        (*tmp).line = NULL;
        CsvTable *buf = tmp;
        tmp = tmp->next;
        if(buf!=NULL)free(buf);
        cpt++;
    }while(tmp!=NULL);
}

FILE *open_file(char * file_name, int mode){
    FILE *file = NULL;

    if((file = fopen(file_name, mode ? "r+" : "w+"))== NULL){
        fprintf(stderr, "Erreur ouverture fichier : %s\n", file_name);
        return NULL;
    }
    return file;
}

CsvTable *initTable(){
    CsvTable *result = (CsvTable*)malloc(sizeof(CsvTable));
    if(result == NULL) return NULL;
    result->nb_cols = 0;
    result->line = NULL;
    result->next = NULL;
    return result;
}


int selectprocess(CsvSelect *select, int actual_field){
    if (select->cols[0] == DEFAULT){ // if all columns get everything
        return 1;
    }else{ // check all columns with parameters
        for(int i =0; i< select->nb_cols;i++){
            if(select->cols[i]==actual_field) return 1;
        }
    }
    return 0;
}

/**
 * \brief obtain columns values, creating and filling CsvTable if matching with where
 * 
 * \param string the line to analyse
 * \param col list of column to get, must end with NULL value, or ONLY DEFAULT value
 * \return char** 
 */
CsvTable *n_field(char *string,CsvSelect *select, CsvWhere where){
    CsvTable *result = initTable();
    if(result == NULL) goto free_error;
    int field_count = 0;
    int in_double_quotes = 0;
    int token_pos = 0;
    int i = 0;
    char token[BUFSIZ];
    int check = 0;
     do {
        token[token_pos++] = string[i];
        if ((!in_double_quotes && (string[i] == CCSV_DELIM || string[i] == '\n' || string[i+1] == '\0')) 
            || (in_double_quotes && string[i] == '"' && string[i+1] == '\0')) { //si la chaine est bien un champ
            if(string[i] == '"') {
                in_double_quotes = !in_double_quotes;
                token_pos--;
            }
            if(string[i+1] == '\0' && string[i] == CCSV_DELIM) token[token_pos-1]=0;
            else if(string[i+1] == '\0' && string[i] != '\n') token[token_pos] = 0;
            else token[token_pos-1] = 0;
            token_pos = 0;
            field_count++;
            if(checkWhere(where,field_count,token)) check =1;
            if(selectprocess(select,field_count)){
                result->nb_cols++;
                result->line=realloc((*result).line,(result->nb_cols)*sizeof(void*));
                result->line[result->nb_cols-1] = (char*)malloc((strlen(token)+1)*sizeof(char));
                if(result->line[result->nb_cols-1] == NULL) goto free_error;
                strcpy(result->line[result->nb_cols-1],token);
                result->line[result->nb_cols-1][strlen(token)]='\0';
            }
        }
        if (string[i] == '"' && string[i + 1] != '"') {
            token_pos--;
            in_double_quotes = !in_double_quotes;
        }
        if (string[i] == '"' && string[i + 1] == '"')
            i++;
    } while (string[++i]);
    free_error:
    if(!check){
        if(result !=NULL)freeTable(result);
        result=NULL;
    }
    return result;
}
// return 0 if false
int checkWhere(CsvWhere where,int col,char *token){
    int check_field = where.field == NULL || !strcmp(where.field,token);
    int check_col = where.col == DEFAULT || where.col == col;
    return check_field  * check_col;
}

/**
 * \brief SELECT selectcols FROM file_name WHERE where
 * 
 * \param selectcols the line will contains these columns
 * \param file_name the function will search in this file
 * \param where the line must respect this struct
 * \return char***, a 3 dimention char, respoecting : lines -> columns -> value
 */
CsvTable *select_csv(CsvSelect *selectcols,char *file_name, CsvWhere where){
    FILE *file = open_file(file_name,READ);
    CsvTable *result = NULL;
    if (file == NULL)goto free_error;
    int row_count = 0;
    char buf[BUFSIZ];
    
    CsvTable *tmp = NULL;
    while (fgets(buf, BUFSIZ, file)) {
        row_count++;
        if (row_count == 1) //on récupère la deuxième ligne du csv
            continue;
        else if(where.line == row_count || where.line == DEFAULT){ // si la ligne recherchée est précisée
        //alors on fait une recherche dans les lignes 
            if(result == NULL) { // si le résultat est nul alors on le remplit par la première valeur
                result = n_field(buf,selectcols,where);
                if(result != NULL){
                    result->line_number = row_count;
                    tmp = result; // on place l'itérateur
                }
            }else { // sinon
                tmp->next = n_field(buf,selectcols,where); // on remplit la prochaine itération
                if(tmp->next != NULL){ // si la valeur ajoutée n'est pas nulle,
                    //alors on déplace l'itérateur
                    tmp->next->line_number = row_count;
                    tmp = tmp->next;
                }
            }
        }
    }
    free_error:
    if(file!= NULL)fclose(file);
    return result;
}


int set_field(FILE *file, char **string, int num_field, char *value){
    char *field = strsep(string, SCSV_DELIM);
    int i = 0;
    int status = 0;
    char *final_value = translateToCsvData(value);
    int size = 1;
    char *line = (char*) malloc(size);
    if(line == NULL) goto free_error;
    line[0] = '\0';
    do{
        if(i){
            size += sizeof(CCSV_DELIM);
            line = realloc(line,size);
            if(line == NULL) goto free_error;
            strcat(line,SCSV_DELIM);
            
        }
        if(i == num_field){
            size += strlen(final_value);
            line = realloc(line,size);
            if(line == NULL) goto free_error;
            strcat(line,final_value);
            status++;
        } else {
            size += strlen(field);
            line = realloc(line,size);
            if(line == NULL) goto free_error;
            strcat(line,field);
        }
        
        i ++;
    } while ((field = strsep(string,SCSV_DELIM)) != NULL);

    if(!status){
        *(line + (strlen(line)-1)) = '\0';
        size += sizeof(CCSV_DELIM);
        size += strlen(value);
        line = realloc(line,size);
        if(line == NULL) goto free_error;
        strcat(line,SCSV_DELIM);
        strcat(line,value);
    }
    if(strchr(line,'\n')==NULL){
        size++;
        line = realloc(line,size);
        if(line == NULL) goto free_error;
        strcat(line,"\n");
    }
    int retcode = fprintf(file, "%s", line) > 0 ? 0 : ERR_TEMP;
    free_error:
    free(line);
    free(final_value);
    return retcode;
}

int set_line(FILE* file,char* file_name,CsvWhere position, char *value){
    FILE *tmp = NULL;
    char *store = NULL;
    char *tmpfile = malloc(strlen(file_name)+1);
    if(tmpfile == NULL) goto free_error;
    strcpy(tmpfile,file_name);
    tmpfile[strlen(file_name)-3] = 't';
    tmpfile[strlen(file_name)-2] = 'm';
    tmpfile[strlen(file_name)-1] = 'p';
    tmpfile[strlen(file_name)] = '\0';
    int i = 0, status = 0;
    store = malloc(BUFSIZ);
    if(store == NULL) goto free_error;
    if( (tmp = open_file(tmpfile, TEMP))== NULL){
        printf("Ecriture dans le fichier %s impossible\n", file_name);
        return ERR_WRITE;
    }
    lock_file(tmp);
    while(fgets(store, BUFSIZ, file) != NULL){
        int rightline = (i == position.line || position.line == DEFAULT); // case when we edit right line
        if(rightline && position.col != DEFAULT){
            //case when we edit right line and
            //when column is specified, juste replace the position
            char * tmpstore = store;
            status = set_field(tmp, &tmpstore,  position.col, value);
        }else if(rightline&&((position.field != NULL&&strstr(store,position.field)!=NULL) 
                            || position.field == NULL)) {
            //case when we edit right line and
            //(field is found) or (column is default and no field)
            //then replace the line
            fprintf(tmp, "%s", value);
            status ++;
        }else{
            //otherwise restore ancient value 
            fprintf(tmp, "%s",store);
        }
        i ++;
    }
    unlock_file(tmp);
    rename(tmpfile,file_name);
    free_error:
    if(tmp != NULL)fclose(tmp);
    if(store != NULL)free(store);
    if(tmpfile != NULL)free(tmpfile);
    return status;
}

int rwrite_csv(char *file_name, CsvWhere position, char *value, int lock){
    FILE *file = NULL;
    int status = 0;
    if( (file = open_file(file_name, READ))== NULL){
        printf("Ecriture dans le fichier %s impossible\n", file_name);
        return ERR_WRITE;
    }
    if(lock)lock_file(file);
    if(position.line == DEFAULT && position.col == DEFAULT && position.field == NULL && position.and == NULL){
        //if all default then just add line
        fseek(file, 0, SEEK_END);
        long length = ftell(file);
        fseek(file, (length - 1), SEEK_SET);
        char c = (char) fgetc(file);
        fseek(file, 0, SEEK_END);
        if(c == '\n')fprintf(file,"%s",value);
        else fprintf(file, "\n%s",value);
        status ++;
    } else {
        status = set_line(file,file_name,position, value);
    }
    if(lock)unlock_file(file);
    fclose(file);
    return status;
}

int write_csv(char *file_name, CsvWhere position, char *value){
    return rwrite_csv(file_name,position,value,0);
}

int write_secure_csv(char *file_name, CsvWhere position, char *value){
    return rwrite_csv(file_name,position,value,1);
}

/**
 * \brief Allows to delete values, if position is default for all field DELETE the FILE entierly
 * Note : don't forget to remove lines of deleted file in other files like ACCESSFILE
 * 
 * \param file_name 
 * \param position 
 * \param value 
 * \return int 
 */
int delete_csv(char *file_name, CsvWhere position){
    FILE *file = NULL;
    int status = 0;
    if( (file = open_file(file_name, READ))== NULL){
        printf("Ecriture dans le fichier %s impossible\n", file_name);
        return ERR_WRITE;
    }
    lock_file(file);
    if(position.line != DEFAULT || position.col != DEFAULT || position.field != NULL){
        status = set_line(file,file_name,position, "");
        unlock_file(file);
        fclose(file);
    }else{
        //then simply delete file...
        unlock_file(file);
        fclose(file);
        remove(file_name);
        status++;
    }
    
    return status;
}