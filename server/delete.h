#ifndef DELETE_H_
#define DELETE_H_

#include "data.h"

/**
 * \brief Route le traitement de la requete vers les sous-traitements
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveru au client
 */
char *traitementDELETE();

#endif