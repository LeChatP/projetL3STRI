#ifndef CONNECT_H_
#define CONNECT_H_

#include "data.h"

/**
 * \brief Traite les connexions utilisateurs
 * \author Eddie BILLOIR
 * 
 * \return char* renvoie la réponse pour les utilisateurs
 */
char *traitementCONNECT();

/**
 * \brief Traite les déconnexions utilisateurs
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *traitementDISCONNECT();

#endif