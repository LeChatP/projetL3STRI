#include "data.h"

int gDisconnected = 1;
char *gUsername = NULL;
USERTYPE gEtat = UNKNOWN;
CsvWhere whereDefault = {.line = DEFAULT,.col=DEFAULT,.field=NULL,.and=NULL};

int getDisconnected(){
    return gDisconnected;
}
void setDisconnected(int value){
    gDisconnected = value;
}

char *getUsername(){
    return gUsername;
}
void setUsername(char *username){
    if(gUsername != NULL) freeUsername();
    gUsername = strdup(username);
}

void freeUsername(){
    if(gUsername != NULL)free(gUsername);
    gUsername = NULL;
}

USERTYPE getUsertype(){
    return gEtat;
}
void setUsertype(USERTYPE type){
    gEtat = type;
}

CsvWhere getWhereDefault(){
    return whereDefault;
}

int isOwner(char *id){
  int res = 0;
  char * path = getBook(id);
  if(access( path, F_OK ) != -1){
    res=1;
  }
  free(path);
  return res;
}

char *getBook(char *idBook){
  char * path = malloc(sizeof(BOOKDIR)+strlen(idBook)+sizeof(CSVEXTENSION));
  if(path == NULL)return NULL;
  sprintf(path,"%s%s%s",BOOKDIR,idBook,CSVEXTENSION);
  return path;
}

char *accessDeniedMessage(){
  return buildMessage(CERR_ACCESS_DENIED,"ACCESS DENIED",NULL);
}