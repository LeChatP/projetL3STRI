#define _GNU_SOURCE
#include "access.h"
#include "connect.h"
#include "create.h"
#include "delete.h"
#include "update.h"
#include "serveur.h"
#include "get.h"
#include "csvManager.h"
#include <dirent.h>
#include <math.h>

#define M_CONNECT 1
#define M_DISCONNECT 2
#define M_GET 3
#define M_CREATE 4
#define M_UPDATE 5
#define M_DELETE 6
#define M_ACCESS 7

int analyseMethode(const char * requete);

void replaceLineInFile(char *filename, char *needle, char* haystack);

char *accessDeniedMessage();

int main() {
  Initialisation();
  while(1){
    
    AttenteClient();
#ifndef DEBUG
    pid_t pid = fork(); /* plus sécurisé que les thread */
#else
    pid_t pid = 0;
#endif

    if(pid == 0){
      while (getDisconnected()) {
        char *reponse = NULL;
        char * request = Reception();/* on place la reception dans la case */
        char *tmp = request;
        char *gUsername = getUsername();
        USERTYPE gEtat = getUsertype();
        if(request == NULL) {
          setDisconnected(0);
          goto free_error;
        }
        switch (analyseMethode(firstArgument(&tmp))) /*  on analyse la première ligne */
        {
          case M_ACCESS:
            if(gUsername != NULL && (gEtat == OWNER || gEtat == OWNER*ADMIN)){
              reponse = traitementACCESS();
            }else reponse = accessDeniedMessage();
          break;
          case M_CONNECT:
            if(gUsername){
              reponse = buildMessage(CERR_ALREADY_CONNECTED,"ALREADY CONNECTED",NULL);
            }else reponse = traitementCONNECT();
          break;
          case M_CREATE:
            if(gUsername) reponse = traitementCREATE();
            else reponse = accessDeniedMessage();
          break;
          case M_DELETE:
            if(gUsername && (gEtat == OWNER || gEtat == USER*ADMIN || gEtat == OWNER*ADMIN)) reponse = traitementDELETE(request);
            else reponse = accessDeniedMessage();
          break;
          case M_DISCONNECT:
            if(gUsername)reponse = traitementDISCONNECT();
            else reponse = accessDeniedMessage();
          break;
          case M_GET:
            if(gUsername)reponse = traitementGET();
            else reponse = accessDeniedMessage();
          break;
          case M_UPDATE:
            if(gUsername)reponse = traitementUPDATE();
            else reponse = accessDeniedMessage();
          break;
          default:
            reponse = buildMessage(CERR_UNKOWN_REQUEST,"UNKOWN REQUEST",NULL);
          break;
        }
        if(!Emission(reponse))setDisconnected(0);
        free_error:
        if(reponse != NULL) free(reponse);
        reponse=NULL;
        if(request != NULL)free(request);
      }
      TerminaisonClient();
      freeUsername();
#ifndef DEBUG
    exit(0);
#endif
    }
    
  }

  return 0;
}

/* retourne 0 si la méthode n'existe pas ou est mal formée */
/* retourne 1 si corresspond à une méthode */
int analyseMethode(const char * requete) {
  int ret = 0; /* compteur de méthodes, et valeur de retour */
  switch ( * requete) /*  Optimisation : On cherche si le premier caractère correspond */
  {
    case 'C':
      if (strcmp(requete + 1, "ONNECT") == 0) {
        ret = M_CONNECT;
      }else if(!strcmp(requete+1,"REATE")){
        ret = M_CREATE;
      }
      break;
    case 'D':
      if (strcmp(requete + 1, "ISCONNECT") == 0) {
        ret = M_DISCONNECT;
      } else if (strcmp(requete + 1, "ELETE") == 0) {
        ret = M_DELETE;
      }
      break;
    case 'A':
      if (strcmp(requete + 1, "CCESS") == 0) {
        ret = M_ACCESS;
      }
      break;
    case 'U':
      if (strcmp(requete + 1, "PDATE") == 0) {
        ret = M_UPDATE;
      }
      break;
    case 'G':
      if (strcmp(requete + 1, "ET") == 0) {
        ret = M_GET;
      }
  default:
    break; /*  aucune Méthode ne correspond */
  }
  return ret;
}



