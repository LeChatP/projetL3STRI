#define _GNU_SOURCE
#ifndef CSVMANAGER_H_
#define CSVMANAGER_H_

/**
 * \file modify_csv.h
 * \author Eddie Billoir (eddie.billoir\gmail.com)
 * \brief Système de gestion du CSV, permettant d'effectuer des requêtes (simples)
 * \date 2020-01-12
 * 
 * \copyright Copyright (c) 2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common.h"

#define ERR_READ 1
#define ERR_WRITE 2
#define ERR_TEMP 3

#define DEFAULT -1
#define READ 1
#define WRITE 1
#define TEMP 0
#define LENGHT_LINE 50
#define LENGHT_FIELD 160


typedef struct sCsvWhere{
    int line ;
    int col ;
    char *field;
    struct sCsvWhere *and;
} CsvWhere;

typedef struct sCsvSelect{
    int cols[MAX_COLUMNS];
    int nb_cols;
} CsvSelect;

typedef struct sCsvTable CsvTable;
struct sCsvTable{
    int line_number;
    char **line;
    int nb_cols;
    CsvTable *next;
};

/**
 * \brief Initialise une ligne du tableau (ou le début du tableau si inexistant)
 * \author Eddie BILLOIR
 * 
 * \return CsvTable* 
 */
extern CsvTable *initTable();

/**
 * \brief SELECT selectcols FROM file_name WHERE where, where unique seulement
 * \author Eddie BILLOIR
 * 
 * \param selectcols 
 * \param file_name 
 * \param where 
 * \return CsvTable* 
 */
extern CsvTable *select_csv(CsvSelect *selectcols,char *file_name, CsvWhere where);

/**
 * \brief Ecrit des données dans un endroit précis dans le csv
 * \note WHERE line et col commencent à 0 
 * \author Eddie BILLOIR
 * 
 * \param file_name 
 * \param position 
 * \param value 
 * \return int 
 */
extern int write_csv(char *file_name, CsvWhere position, char *value);

/**
 * \brief effectue l'écriture comme write_csv mais en bloquant en écriture les autres processus
 * \note incompatible avec les systèmes NFS, WHERE line et col commencent à 0 
 * \author Eddie BILLOIR
 * 
 * \param file_name 
 * \param position 
 * \param value 
 * \return int 
 */
extern int write_secure_csv(char *file_name, CsvWhere position, char *value);

/**
 * \brief Allows to delete values, if position is default for all field DELETE the FILE entierly
 * \note don't forget to remove lines of deleted file in other files like ACCESSFILE
 * 
 * \param file_name 
 * \param position 
 * \param value 
 * \return int 
 */
int delete_csv(char *file_name, CsvWhere position);

/**
 * \brief free all dynamic memory of table
 * 
 * \param table 
 */
extern void freeTable(CsvTable *table);

#endif