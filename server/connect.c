#include "connect.h"

/**
 * \brief Obtient les informations utilisateur
 * 
 * \param user nom utilisateur.
 * \param adminfield champ administrateur du CSV
 * \return char* la réponse du serveur pour le client
 */
char *getUserConnectInfo(char *user,char *adminfield);

char *getUserConnectInfo(char *user,char *adminfield){
  char *usertype = NULL;
  char *admin = NULL;
  char *book = getBook(user);
  if(!access(book,F_OK)){
    usertype = "OWNER";
    setUsertype(OWNER);
  }else{
    usertype = "USER";
    setUsertype(USER);
  }
  if(*adminfield == '1'){
    admin = "ADMIN";
    setUsertype(getUsertype()*ADMIN);
  }else{
    admin = "";
  }
  setUsername(user);
  free(book);
  return buildMessage(COK,"CONNECTED",usertype,admin,NULL);
}

char *traitementCONNECT(){
  char *username = nextArgument();
  char *pass = nextArgument();
  char *result = NULL;
  CsvSelect select = {.cols = {DEFAULT}, .nb_cols = 1};
  CsvWhere where = { .col = 1, .line = DEFAULT };
  where.field = username;
  CsvTable *results = select_csv(&select,USERSFILE,where);
  CsvTable *tmp = results;
  if(results == NULL) goto free_rscs;
  do{
    if(!strcmp(tmp->line[1],"")){
      where.line = tmp->line_number-1;
      where.col = 1;
      write_secure_csv(USERSFILE,where,pass);
      result = getUserConnectInfo(username,tmp->line[2]);
      goto free_rscs;
    }
    if(!strcmp(tmp->line[1],pass)){
      result = getUserConnectInfo(username,tmp->line[2]);
      goto free_rscs;
    }
    tmp=tmp->next;
  }while(tmp != NULL);
  result = buildMessage(CERR_WRONG_CREDENTIALS,"INVALID USER OR PASSWORD",NULL);
  free_rscs:
  if(result == NULL) result = buildMessage(CERR_INEXISTENT_USER,"USER NOT IN DATABASE",NULL);
  if(results != NULL) freeTable(results);
  return result;
}
char *traitementDISCONNECT(){
  setDisconnected(0);
  return NULL;
}