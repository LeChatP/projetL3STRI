#include "access.h"

/**
 * \brief Effectue le traitement pour l'autorisation des accès
 * \author Eddie BILLOIR
 * 
 * \param id identifiant utilisateur a autoriser
 * \return char* réponse à envoyer au serveur
 */
char *traitementACCESSGRANT(char *id);

/**
 * \brief Effectue le traitement pour la révocation des accès
 * \author Eddie BILLOIR
 * 
 * \param id identifiant utilisateur a autoriser
 * \return char* réponse à envoyer au serveur
 */
char *traitementACCESSREVOKE(char *id);

/**
 * \brief Vérifie si l'utilisateur possède un droit de lecture sur l'annuaire du propriétaire
 * \author Eddie BILLOIR 
 * 
 * \param owner propriétaire
 * \param user utilisateur
 * \return int 1 si oui 0 si non
 */
int isGranted(char* owner,char* user);

int hasAccess(char *idAnnuaire){
    char * username = getUsername();
    return isGranted(idAnnuaire,username) || !strncmp(idAnnuaire,username,strlen(username));
}

int exist(char* user,char* file){
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col = 1,  .line = DEFAULT ,.field = user};
  CsvTable *results = select_csv(&select,file,where);
  int result = results != NULL && results->line_number != 0;
  freeTable(results);
  return result;
}

int isGranted(char* owner,char* user){
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col = 2,  .line = DEFAULT, .field = user };
  CsvTable *results = select_csv(&select,ACCESSFILE,where);
  int granted = 0;
  if (results == NULL)
    return granted;
  CsvTable *tmp = results;
  while(tmp != NULL){
    if(!strcmp(tmp->line[0],owner)) granted ++;
    tmp = tmp->next;
  }
  freeTable(results);
  return granted;
}

char *traitementACCESS(){
  char *requeteType = nextArgument();
  if(!strcmp(requeteType,"GRANT")){
    return traitementACCESSGRANT(nextArgument());
  }else if(!strcmp(requeteType,"REVOKE")){
    return traitementACCESSREVOKE(nextArgument());
  }else return buildMessage(CERR_UNKOWN_REQUEST,"UNKWOWN ACCESS REQUEST",NULL);
}


char *traitementACCESSGRANT(char *id){
  char * reponse;
  char* username = getUsername();
  if(exist(id,USERSFILE)){
    if(!isGranted(username,id)){
      // add access
      CsvWhere where = getWhereDefault();
      char *line = buildCsvLine(username,id,NULL);
      write_csv(ACCESSFILE,where,line);
      reponse = buildMessage(COK,"OK",NULL);
    }else{
      reponse = buildMessage(CERR_ALREADY_IN_LIST,"ALREADY IN LIST",NULL);
    }
  }else{
    reponse = buildMessage(CERR_INEXISTENT_USER,"INEXISTANT USER",NULL);
  }
  return reponse;
}
char *traitementACCESSREVOKE(char *id){
  char * reponse;
  char *username = getUsername();
  if(isGranted(username,id)){
    // add access
    char *line = buildCsvLine(username,id,NULL);
    CsvWhere where = { .col = DEFAULT,  .line = DEFAULT,.field = line};
    if(delete_csv(ACCESSFILE,where))
      reponse = buildMessage(COK,"OK",NULL);
    else reponse = buildMessage(CERR_UNABLE_EXEC,"UNABLE TO DELETE USER",NULL);
  }else{
    reponse = buildMessage(CERR_NOT_IN_LIST,"NOT IN LIST",NULL);
  }
  return reponse;
}