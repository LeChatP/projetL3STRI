#include "get.h"

/**
 * \brief Traite les requêtes pour obtenir la liste des Annuaires
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveru au client
 */
char* traitementGETBOOKLIST();

/**
 * \brief Traite les requêtes pour obtenir la liste des contacts d'un annuaire
 * \author Eddie BILLOIR
 * 
 * \param id L'identifiant de l'annuaire
 * \return char* la réponse du serveur au client
 */
char* traitementGETBOOK(char *id);

/**
 * \brief Traite les requetes pour obtenir tous les utilisateur du serveur
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char* traitementGETSERVER();

/**
 * \brief Traite les requetes pour obtenir les informations utilisateur
 * \author Eddie BILLOIR
 * 
 * \param id 
 * \return char* la réponse du serveur au client
 */
char* traitementGETUSER(char *id);

/**
 * \brief Traite les requestes pour obtnir les utilisateur ayant accès a l'annuaire
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char* traitementGETUSERS();

/**
 * \brief Obtient les informations d'un contact
 * \author Eddie BILLOIR
 * 
 * \param idAnnuaire l'annuaire
 * \param idContact le contact
 * \return char* la réponse du serveur au client
 */
char* traitementGETCONTACT(char *idAnnuaire,char*idContact);

char *traitementGET(){
  char *requeteType = nextArgument();
  if(!strcmp(requeteType,"BOOKLIST"))return traitementGETBOOKLIST(nextArgument());
  else if(!strcmp(requeteType,"BOOK")){
    char *owner = nextArgument();
    if(hasAccess(owner))return traitementGETBOOK(owner);
    else return accessDeniedMessage();
  }else if(!strcmp(requeteType,"CONTACT")){
    char *bookid = nextArgument();
    char *contactid = nextArgument();
    if(hasAccess(bookid))return traitementGETCONTACT(bookid,contactid);
    else return accessDeniedMessage();
  }
  else if(!strcmp(requeteType,"SERVER")){
    if(getUsertype() > 3)return traitementGETSERVER();
    else return accessDeniedMessage();
  }
  else if(!strcmp(requeteType,"USERS")) if(isOwner(getUsername()))return traitementGETUSERS();
    else return accessDeniedMessage();
  else if(!strcmp(requeteType,"USER"))return traitementGETUSER(nextArgument());
  else return buildMessage(CERR_UNKOWN_REQUEST,"UNKWOWN GET REQUEST",NULL);
}

char* traitementGETBOOKLIST(){
  char * reponse = buildPartMessage(CID_BOOK,"BOOKLIST",NULL);
  //check himself book
  char *username = getUsername();
  if(isOwner(username)){
    reponse = appendMessageData(reponse,username);
  }
  //check oher books
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col = 2, .line = DEFAULT, .field=username };
  CsvTable *results = select_csv(&select,ACCESSFILE,where);
  if(results == NULL){
    goto free_error;
  }
  CsvTable *tmp = results;
  while(tmp != NULL){
    reponse = appendMessageData(reponse,tmp->line[0]);
    tmp=tmp->next;
  }
  free_error:
  reponse = endMessageLine(reponse);
  if(results!=NULL)freeTable(results);
  return reponse;
}
char* traitementGETBOOK(char *id){
  CsvTable *results = NULL;
  char * reponse = NULL;
  char * path = getBook(id);
  if(access(path,F_OK)) goto free_error;
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = getWhereDefault();
  results = select_csv(&select,path,where);
  if(results == NULL) goto free_error;
  reponse = buildPartMessage(CID_CONTACT,"CONTACTLIST",NULL);
  CsvTable *tmp = results;
  while(tmp != NULL){
    reponse = appendMessageData(reponse,tmp->line[0]);
    tmp=tmp->next;
  }
  reponse = endMessageLine(reponse);
  free_error:
  if(reponse == NULL) reponse = buildMessage(CID_CONTACT,"CONTACTLIST",NULL);
  if(results != NULL)freeTable(results);
  if(path != NULL)free(path);
  return reponse;
}

char* traitementGETSERVER(){
  char * reponse = NULL;
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = getWhereDefault();
  CsvTable *results = select_csv(&select,USERSFILE,where);
  if(results == NULL) goto free_error;
  CsvTable *tmp = results;
  reponse = buildPartMessage(CID_USER,"USERLIST",NULL);
  while(tmp != NULL && tmp->line != NULL){
    reponse = appendMessageData(reponse,tmp->line[0]);
    tmp=tmp->next;
  }
  reponse = endMessageLine(reponse);
  free_error:
  if(reponse == NULL) reponse= buildMessage(CERR_UNABLE_EXEC,"NO USERS IN DATABASE"); //impossible normalement
  if(results != NULL)freeTable(results);
  return reponse;
}

char* traitementGETUSER(char *id){
  char * code, *message, *field;
  char * reponse = NULL;
  CsvTable *results = NULL;
  if(id == NULL || strcmp(getUsername(),id) == 0) {
    message = "YOURINFO";
    code = CDATA_CLIENT;
    field = getUsername();
  }else {
    code = CDATA_USER;
    message = "USERINFO";
    field = id;
  }
  CsvSelect select = {.cols = {DEFAULT}, .nb_cols = 1};
  CsvWhere where = { .col = 1, .line = DEFAULT, .field = field };
  results = select_csv(&select,USERSFILE,where);
  if(results == NULL){
    goto free_error;
  }
  reponse = buildPartMessage(code,message,NULL);
  CsvTable *tmp = results;
  for(int i = 3;i<tmp->nb_cols;i++) reponse = appendMessageData(reponse,tmp->line[i]);
  reponse = endMessageLine(reponse);
  free_error:
  if(results == NULL) reponse = buildMessage(CERR_INEXISTENT_USER,"INEXISTANT USER");
  freeTable(results);
  return reponse;
}

char* traitementGETUSERS(){
  CsvTable *results = NULL;
  char * reponse = NULL;
  CsvSelect select = {.cols = {2}, .nb_cols = 1};
  CsvWhere where = {.col = 1, .line = DEFAULT,.field = getUsername() };
  results = select_csv(&select,ACCESSFILE,where);
  if(results == NULL) goto free_error;
  CsvTable *tmp = results;
  reponse = buildPartMessage(CID_USER,"USERLIST",NULL);
  while(tmp != NULL){
    reponse = appendMessageData(reponse,tmp->line[0]);
    tmp=tmp->next;
  }
  reponse = endMessageLine(reponse);
  free_error:
  if(reponse == NULL) reponse= buildMessage(CID_USER,"USERLIST",NULL);
  freeTable(results);
  return reponse;
}

char* traitementGETCONTACT(char *idBook,char *idContact){
  if(idBook == NULL || idContact == NULL ){
    return buildMessage(CERR_FEW_ARGUMENTS,"NOT ENOUGH ARGUMENTS");
  }
  char * reponse = NULL;
  char *path = getBook(idBook);
  CsvTable *results = NULL;
  if(path == NULL) goto free_error;
  CsvSelect select = {.cols = {DEFAULT}, .nb_cols = 1};
  CsvWhere where = { .col = 1, .line = DEFAULT, .field=idContact };
  results = select_csv(&select,path,where);
  if(results == NULL)goto free_error;
  reponse = buildPartMessage(CDATA_CONTACT,"CONTACTINFO",NULL);
  CsvTable *tmp = results;
  for(int i = 1;i<tmp->nb_cols;i++) reponse = appendMessageData(reponse,tmp->line[i]);
  reponse = endMessageLine(reponse);
  free_error:
  if(reponse == NULL) reponse= buildMessage(CERR_UNABLE_EXEC,"EMPTY DATA",NULL);
  freeTable(results);
  free(path);
  return reponse;
}