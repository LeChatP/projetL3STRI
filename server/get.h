#ifndef GET_H_
#define GET_H_

#include "./data.h"
#include "access.h"

/**
 * \brief Traite toutes les requetes GET
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur envoyée au client
 */
char *traitementGET();

#endif