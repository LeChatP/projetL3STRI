#include "update.h"

/**
 * \brief Traite les requetes de demande de mise a jour d'un uilisateur
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementUPDATEUSER();

/**
 * \brief Traite les mises à jour des contacts
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementUPDATECONTACT();

/**
 * \brief Vérifie si la chaine est vide
 * 
 * \param field la chaine vérifiée
 * \return int 0 si vide 1 si pas vide
 */
int checkFilled(char *field);

/**
 * \brief Met a jour un champ dans un fichier
 * 
 * \param field le champ a modifier
 * \param file le fichier
 * \param where La localisation dans le fichier
 * \return int 
 */
int updateField(char *field,char *file,CsvWhere where);

char *traitementUPDATE(){
  char *requeteType = nextArgument();
  if(!strcmp(requeteType,"USER")){
    if(getUsertype() > 3)return traitementUPDATEUSER();
    else return accessDeniedMessage();
  }else if(!strcmp(requeteType,"CONTACT")){
    if(isOwner(getUsername()))return traitementUPDATECONTACT();
    else return accessDeniedMessage();
  }else return buildMessage(CERR_UNKOWN_REQUEST,"UNKWOWN UPDATE REQUEST",NULL);

}

int checkFilled(char *field){
  return field != NULL && strcmp(field,"");
}

int updateField(char *field,char *file,CsvWhere where){
  int res = 0;
  if(checkFilled(field)){
    res = write_secure_csv(file,where,field);
  }
  return res;
}

char *traitementUPDATEUSER(){
  User user;
  user.userName = nextArgument();
  user.nom = nextArgument();
  user.prenom = nextArgument();
  user.email = nextArgument();
  char *reset = nextArgument(); 
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col=1, .line = DEFAULT,.field = user.userName };
  CsvTable *results = select_csv(&select,USERSFILE,where);
  char *reponse = NULL;
  if(results == NULL)goto free_error;
  where.line = results->line_number-1;
  where.field = NULL;
  if(!strcmp(reset,"RESET")){ // supprimer le mot de passe si besoin
    where.col = 1;
    delete_csv(USERSFILE,where);
  }
  where.col=3;
  updateField(user.nom,USERSFILE,where);
  where.col++;
  updateField(user.prenom,USERSFILE,where);
  where.col++;
  updateField(user.email,USERSFILE,where);
  reponse = buildMessage(COK,"USER UPDATED",NULL);
  free_error:
  freeTable(results);
  if(reponse == NULL) buildMessage(CERR_UNABLE_EXEC,"UNABLE TO EXEC");
  return reponse;
}

char *traitementUPDATECONTACT(){
  Contact contact;
  //<id>/<nom>/<prénom>/<mail>/<naissance>/<téléphone>/<code_postal>/<ville>/<adresse>/<remarque>
  contact.contactID = nextArgument();
  contact.nom = nextArgument();
  contact.prenom = nextArgument();
  contact.mail = nextArgument();
  contact.naissance = nextArgument();
  contact.telephone = nextArgument();
  contact.codep = nextArgument();
  contact.ville = nextArgument();
  contact.adresse = nextArgument();
  contact.remarque = nextArgument();
  char *reponse;
  char *book = getBook(getUsername());
  if(book == NULL) goto free_error;
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col = 1, .line = DEFAULT, .field = contact.contactID };
  CsvTable *results = select_csv(&select,book,where);
  if(results == NULL) goto free_error;
  where.line = results->line_number-1;
  where.field = NULL;
  where.col = 1;
  updateField(contact.nom,book,where);
  where.col++;
  updateField(contact.prenom,book,where);
  where.col++;
  updateField(contact.mail,book,where);
  where.col++;
  updateField(contact.naissance,book,where);
  where.col++;
  updateField(contact.telephone,book,where);
  where.col++;
  updateField(contact.codep,book,where);
  where.col++;
  updateField(contact.ville,book,where);
  where.col++;
  updateField(contact.adresse,book,where);
  where.col++;
  updateField(contact.remarque,book,where);
  reponse = buildMessage(COK,"CONTACT UPDATED",NULL);
  free_error:
  if(results != NULL)freeTable(results);
  if(reponse == NULL) buildMessage(CERR_UNABLE_EXEC,"UNABLE TO EXECUTE");
  if(book != NULL)free(book);
  return reponse;
}
