#include "delete.h"

/**
 * \brief Supprime un utilisateur si autorisé
 * \author Eddie BILLOIR
 * 
 * \param id Id Utilisateur
 * \return char* La réponse du serveur au client
 */
char *traitementDELETEUSER(char *id);

/**
 * \brief Supprime un contact
 * \author Eddie BILLOIR
 * 
 * \param id 
 * \return char* 
 */
char *traitementDELETECONTACT(char *id);

/**
 * \brief Supprime son Annuaire (utilisateur de la session)
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *traitementDELETEBOOK();

/**
 * @brief Supprime un annuaire avec les accès
 * 
 * @param id Id de l'annuaire
 * @return int 1 si OK 0 si erreur
 */
int deleteBOOK(char *id);


char *traitementDELETE(){
  char * result = NULL;
  char *requeteType = nextArgument();
  USERTYPE gEtat = getUsertype();
  if(!strcmp(requeteType,"BOOK")){
    if(gEtat%OWNER == 0)result = traitementDELETEBOOK();
  }else if(!strcmp(requeteType,"CONTACT")){
    char *id = nextArgument();
    if(gEtat%OWNER == 0)result =  traitementDELETECONTACT(id);
  }else if(!strcmp(requeteType,"USER")){
    char *id = nextArgument();
    if(gEtat > 3)result = traitementDELETEUSER(id);
  }else result = buildMessage(CERR_UNKOWN_REQUEST,"UNKWOWN DELETE REQUEST",NULL);
  if(result ==NULL)result = accessDeniedMessage();
  return result;
}

int deleteBOOK(char *id){
  int result = 0;
  CsvTable *table = NULL;
  char *book = getBook(id);
  if(book != NULL){
    CsvWhere wherea = { .col = 1,  .line = DEFAULT,.field = id};
    CsvSelect select = { .cols = {2}, .nb_cols = 1};
    table = select_csv(&select,ACCESSFILE,wherea);
    if(table != NULL){
      CsvTable *tmp = table;
      wherea.field = NULL;
      wherea.col = DEFAULT;
      int i = 0;
      while(tmp != NULL){
        wherea.line = tmp->line_number-1 -i;
        delete_csv(ACCESSFILE,wherea);
        tmp = tmp->next;
        i++;
      }
      freeTable(table);
    }
  }
  CsvWhere whereb = {.col = DEFAULT,.field=NULL,.line=DEFAULT};
  if(delete_csv(book,whereb)){
    result++;
  }
  if(book !=NULL)free(book);
  return result;
}

char *traitementDELETEUSER(char *id){
  char *reponse = NULL;
  CsvTable *results = NULL;
  if(!strcmp(id,getUsername()))goto free_error;
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col=1, .line = DEFAULT,.field = id };
  results= select_csv(&select,USERSFILE,where);
  if(results == NULL)goto free_error;
  where.line = results->line_number-1;
  where.field = NULL;
  where.col = DEFAULT;
  if(delete_csv(USERSFILE,where)){
    reponse = buildMessage(COK,"USER DELETED",NULL);
    deleteBOOK(id);
  }
  free_error:
  if(reponse == NULL)reponse = buildMessage(CERR_UNABLE_EXEC,"UNABLE TO EXECUTE",NULL);
  if(results != NULL)freeTable(results);
  return reponse;
}
char *traitementDELETECONTACT(char *id){
  char *book = getBook(getUsername());
  char *reponse = NULL;
  CsvTable *results = NULL;
  if(access(book,F_OK))goto free_error;
  CsvSelect select = {.cols = {1}, .nb_cols = 1};
  CsvWhere where = { .col=1, .line = DEFAULT,.field = id };
  results = select_csv(&select,book,where);
  if(results == NULL)goto free_error;
  where.line = results->line_number-1;
  where.field = NULL;
  where.col = DEFAULT;
  delete_csv(book,where);
  reponse = buildMessage(COK,"USER DELETED",NULL);
  free_error:
  if(book != NULL) free(book);
  if(reponse == NULL)reponse = buildMessage(CERR_UNABLE_EXEC,"UNABLE TO EXECUTE",NULL);
  if(results != NULL)freeTable(results);
  return reponse;
}
char *traitementDELETEBOOK(){
  char *reponse = NULL;
  if(deleteBOOK(getUsername())){
    setUsertype(getUsertype() < 4 ? USER : USER*ADMIN);
    reponse = buildMessage(COK,"BOOK DELETED",NULL);
  }
  else reponse = buildMessage(CERR_UNABLE_EXEC,"UNABLE TO DELETE BOOK",NULL);

  return reponse;
}