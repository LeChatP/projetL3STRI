#include "create.h"
#include "access.h"

#include <math.h>

/**
 * \brief Traite la création d'utilisateur
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementCREATEUSER();

/**
 * \brief Traite la création d'un annuaire utilisateur
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementCREATEBOOK();

/**
 * \brief Traite la création d'un contact
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementCREATECONTACT();

/**
 * \brief Crée un identifiant d'utilisateur/ de contact unique
 * \author Eddie BILLOIR
 * 
 * \param nom Nom de l'utilisateur/contact
 * \param prenom Prénom de l'utilisateur/contact
 * \param file fichier des utilisateurs/des contacts
 * \return char* la réponse du serveur au client
 */
char *createUsername(char *nom,char *prenom,char *file);

char *traitementCREATE(){
  char *requeteType = nextArgument();
  if(!strcmp(requeteType,"USER")){
    if(getUsertype() > 3)return traitementCREATEUSER();
    else return accessDeniedMessage();
  }else if(!strcmp(requeteType,"BOOK")){
    return traitementCREATEBOOK();
  }else if(!strcmp(requeteType,"CONTACT")){
    if(isOwner(getUsername()))return traitementCREATECONTACT();
    else return accessDeniedMessage();
  }else return buildMessage(CERR_UNKOWN_REQUEST,"UNKWOWN CREATE REQUEST",NULL);
}

char *createUsername(char *nom,char *prenom,char *file){
  size_t size = strlen(nom)+2;
  char *username = malloc(size);
  if(username == NULL) goto free_error;
  strncpy(username,prenom,1);
  strcpy(username+1,nom);
  username = sanitizeCharTo(username,' ', "_");
  int i = 1;
  while(exist(username,file)){
    int nDigits = floor(log10(abs(i))) + 1;
    username = realloc(username,size+nDigits);
    if(username == NULL)goto free_error;
    sprintf(username+size-1,"%d",i);
    i++;
  }
  free_error:
  if(username == NULL)perror("Impossble d'allouer la mémoire pour un nouvel utilisateur");
  return username;
}

char *traitementCREATEUSER(){
  char *result = NULL;
  char *username = NULL;
  char *line = NULL;
  // on n'enregistre pas les champs dans la structure User, car il faudrait copier le contenu
  User user;
  user.nom = nextArgument(); // ici on manipule simplement les pointeurs
  user.prenom = nextArgument();
  user.email = nextArgument();
  if(user.nom == NULL || user.prenom == NULL){
    result = buildMessage(CERR_FEW_ARGUMENTS,"TOO FEW ARGUMENTS",NULL);
    goto free_error;
  }
  username = createUsername(user.nom,user.prenom,ACCESSFILE);
  line = buildCsvLine(username,"","0",user.nom,user.prenom,user.email,NULL);
  if(write_csv(USERSFILE,getWhereDefault(),line))
    result = buildMessage(COK,"OK",username,NULL);
  else result = buildMessage(CERR_UNABLE_EXEC,"UNABLE TO CREATE USER",NULL);
  free_error:
  if(username != NULL)free(username);
  if(line!=NULL)free(line);
  return result;
}
char *traitementCREATEBOOK(){
  char *book = getBook(getUsername());
  char *reponse = NULL;
  if(access(book,F_OK)){
    FILE *f = fopen(book,"w");
    fprintf(f,"<id>,<nom>,<prénom>,<mail>,<naissance>,<téléphone>,<code_postal>,<ville>,<adresse>,<remarque>");
    fclose(f); // create file
    setUsertype(getUsertype() < 4 ? OWNER : OWNER*ADMIN);
    reponse = buildMessage(COK,"OK",NULL);
  }else{
    reponse = buildMessage(CERR_HAS_BOOK,"BOOK ALREADY CREATED",NULL);
  }
  free(book);
  return reponse;
}
char *traitementCREATECONTACT(){
  Contact contact;
  //<id>/<nom>/<prénom>/<mail>/<naissance>/<téléphone>/<code_postal>/<ville>/<adresse>/<remarque>
  contact.nom = nextArgument();
  contact.prenom = nextArgument();
  contact.mail = nextArgument();
  contact.naissance = nextArgument();
  contact.telephone = nextArgument();
  contact.codep = nextArgument();
  contact.ville = nextArgument();
  contact.adresse = nextArgument();
  contact.remarque = nextArgument();
  char *book = getBook(getUsername());
  char *idclient = createUsername(contact.nom,contact.prenom,book);
  char *contactLine = buildCsvLine(idclient,contact.nom,contact.prenom,contact.mail,contact.naissance,contact.telephone,contact.codep,contact.ville,contact.adresse,contact.remarque,NULL);
  write_csv(book,getWhereDefault(),contactLine);
  free(idclient);
  free(contactLine);
  free(book);
  char *result = buildMessage(COK,"OK",NULL);
  return result;
}