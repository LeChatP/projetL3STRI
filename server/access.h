#ifndef ACCESS_H_
#define ACCESS_H_

#include "data.h"

/**
 * \brief Effectue le traitement des requêtes D'accès
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *traitementACCESS();

/**
 * \brief Recherche les droits d'accès de l'utilisateur de session à un annuaire
 * \author Eddie BILLOIR
 * 
 * \param idAnnuaire l'annuaire demandé
 * \return int 
 */
int hasAccess(char *idAnnuaire);

/**
 * \brief Réponse Accès non autorisé
 * \author Eddie BILLOIR
 * 
 * \return char* 
 */
char *accessDeniedMessage();

/**
 * \brief Vérifie si l'utilisateur existe dans le fichier file
 * \author Eddie BILLOIR
 * 
 * \param user nom d'utilisateur
 * \param file  fichier a rechercher
 * \return int 1 si oui 0 si non
 */
int exist(char* user,char* file);

#endif