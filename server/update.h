#ifndef UPDATE_H_
#define UPDATE_H_

#include "data.h"

/**
 * \brief Traite toutes les requêtes de Mise a jour
 * \author Eddie BILLOIR
 * 
 * \return char* la réponse du serveur au client
 */
char *traitementUPDATE();

#endif