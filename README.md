# ProjetL3STRI

Ce projet est basé sur le modèle client/serveur. Ce projet respecte une RFC définie et spécifiée afin de permettre un système de partage d'annuaires.

## Compilation

Afin de compiler il est nécéssaire de faire :

```Bash
./configure.sh
```

Pour compiler le serveur il faut :

```Bash
make build_server
```

Pour compiler le client il faut :

```Bash
make build_client
```

## Utilisation

Peu importe l'ordre de démarrage, il faut lancer ./cli et ./srv

Une fois lancé l'adresse du serveur est demandé par le client. il faut alors le renseigner. il est possible de spécifier un port en particulier en collant ':'

Le logiciel vous demandera vos identifiants de connexion, une fois connecté vous aurez accès au service.

Lors de la prenière connexion, le mot de passe entré sera celui défini pour l'utilisateur. Seul un administrateur pourra réinitialiser ce mot de passe.

Pour modifier un mot de passe en tant qu'administrateur il faut aller dans le menu modifier un utiisateur et répondre 'y' lorsque le client propose de réinitialiser le mot de passe

Pour autoriser un utiliateur dans son annuaire, il faut connaître son identifiant.

Lors de la création d'un utilisateur, l'identifiant de l'utilisateur est donné a la fin, il faudra le noter et le divulguer a la personne concernée.

Un utilisateur, administrateur ou non n'a par défaut pas d'annuaire, il peut en créer un. Après la création d'un annuaire il deviendra propriétaire et administrateur si il l'était précédemment.

## Cas particuliers

Ce serveur est partiellement incompatible avec un système de fichiers NFS dû à un éventuel accès en concurrence sur des fichiers communs. Cette fonctionnalité est à améliorer. Voir TODO#1

## TODO

1. use fnctl() to lock file compatible with NFS.
